//
//  HomeVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 25/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeTableViewCell.h"
#import "SalesFiltersVC.h"

@interface HomeVC : UIViewController<UISearchBarDelegate,PicResultsAfterFilterDelegate>
{
NSMutableArray * arrayForBackGroundImages;
    NSMutableDictionary *dataFilter;
    BOOL isFromSearch;
    
    BOOL hasSearchResults;
    BOOL isarabic;
}
@property (strong, nonatomic) IBOutlet UITableView *homeTableView;

- (IBAction)btnForSalePrssed:(id)sender;
- (IBAction)btnForRentPressed:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UIImageView *imgSalebtnArrow;
@property (strong, nonatomic) IBOutlet UIImageView *imgRentButtonArrow;

@property NSMutableArray * arrayForBackGroundImages;
@property NSMutableArray * arrayForPrices;
@property NSMutableArray * arrayForNumOfRooms;
@property NSMutableArray * arrayForLocations;
@property NSMutableArray * arrayForTypeOfFlats;

@end
