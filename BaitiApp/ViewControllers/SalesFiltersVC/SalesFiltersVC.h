//
//  SalesFiltersVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NMRangeSlider.h"
@protocol PicResultsAfterFilterDelegate <NSObject>

-(void)setfilteredResults:(NSMutableDictionary*)datatDict;
@end


@class RangeSlider;
@interface SalesFiltersVC : UIViewController<UIGestureRecognizerDelegate>

{
    IBOutlet UIView *viewForSlider;
    IBOutlet UILabel *rangeLabel;
    RangeSlider *slider;
    UILabel *reportLabel;
    
    BOOL bedroomSelected;
    BOOL propertySelected;
    
    BOOL isarabic;
    
}
@property (weak, nonatomic) IBOutlet NMRangeSlider *sliderView;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet UIButton *fristBtn;
@property (strong, nonatomic) IBOutlet UIButton *secondBtn;
@property (strong, nonatomic) IBOutlet UIButton *thirdBtn;
@property (strong, nonatomic) IBOutlet UIButton *fourthBtn;
@property (strong, nonatomic) IBOutlet UIButton *fifthBtn;
@property (strong, nonatomic) IBOutlet UIButton *sixthBtn;
@property (strong, nonatomic) IBOutlet UITextField *txtFldForPicker;
@property (strong, nonatomic)NSString *propertyID;
@property (strong, nonatomic) NSMutableDictionary *dictaFilter;
@property (strong, nonatomic) NSMutableArray *locationArr;
- (IBAction)labelSliderChanged:(NMRangeSlider*)sender;
- (IBAction)firstBtnPressed:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sliderViewVSConst;

@property (assign, nonatomic) id<PicResultsAfterFilterDelegate> filterDelegate;

@end
