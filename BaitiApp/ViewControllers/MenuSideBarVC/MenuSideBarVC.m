//
//  MenuSideBarVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 27/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "MenuSideBarVC.h"
#import "MenuCustemCell.h"
#import "AgentsListVC.h"
#import "MyFavouritesVC.h"
#import "MyListedPropertiesVC.h"
#import "AdevertisementRequestVC.h"
#import "NotificationsVC.h"
#import "SignInVC.h"
#import "MyProfileVC.h"
#import "HomeVC.h"
#import "PostProperty.h"
#import "UIButton+WebCache.h"
#import "SettingsVC.h"


@interface MenuSideBarVC ()<UIAlertViewDelegate>
{
    
    IBOutlet UIButton *LocationIcon;
    IBOutlet UIButton *btnProfileImg;
    IBOutlet UILabel *txtUsername;
    IBOutlet UILabel *txtUserLocation;
    NSDictionary *dataDict;
}
@end

@implementation MenuSideBarVC
@synthesize contactListMA,imagesMA,tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //  _userprofilePic.layer.cornerRadius = _userprofilePic.frame.size.width / 2;
    btnProfileImg.layer.cornerRadius=btnProfileImg.frame.size.width/2;
    btnProfileImg.layer.borderColor=[UIColor colorWithRed:(181.0/255.0f) green:(-0.0/255.0f) blue:(94/255.0f) alpha:1.0f].CGColor;
    btnProfileImg.layer.borderWidth=5.0f;
    btnProfileImg.clipsToBounds = YES;
    contactListMA=[[NSMutableArray alloc]init];
    
    NSString *lang = [[NSUserDefaults standardUserDefaults]
                      objectForKey:@"ICPreferredLanguage"];
    if ([lang isEqualToString:@"ar"])
    {
       
        isArabic=YES;
    }
    else
    {
        isArabic=NO;
    }
    
        contactListMA=[[NSMutableArray alloc]initWithObjects:@"Home",@"Search Agent",@"Favourites",@"My Listed Properties",@"Send Advertising Request",@"My Notifications",@"Login", nil];
    
    //************************ image araay is  *****************
    imagesMA = [[NSMutableArray alloc] initWithObjects:@"homeIcon",@"searchIcon",@"fav_icon-1", @"list_property_icon", @"adver_icon", @"notifaction_icon", @"logout_icon", nil];
    //[self contactListArray];
    

    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    NSData *dictionaryData = [[NSUserDefaults standardUserDefaults] objectForKey:UD_USERINFO];
    dataDict = [NSKeyedUnarchiver unarchiveObjectWithData:dictionaryData];
    if ([self isNotNull:[[NSUserDefaults standardUserDefaults]objectForKey:UD_TOKEN]]) {
        
        NSData *data = [[dataDict objectForKey:@"username"] dataUsingEncoding:NSUTF8StringEncoding];
        txtUsername.text = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
        txtUserLocation.text=[dataDict objectForKey:@"address"];
        [btnProfileImg sd_setImageWithURL:[NSURL URLWithString:[dataDict objectForKey:@"image"]] forState:UIControlStateNormal];

        if (txtUserLocation.text.length>0) {
            [LocationIcon setHidden:NO];
        }
        else
        {
            [LocationIcon setHidden:YES];
        }
    }
    else
    {
  
       txtUsername.text=LOCALIZATION(@"Guest");
        
        NSString *str1=LOCALIZATION(@"No");
        NSString *str2=LOCALIZATION(@"Address");
        txtUserLocation.text=[NSString stringWithFormat:@"%@ %@",str1,str2];
 
        [btnProfileImg setBackgroundImage:[UIImage imageNamed:@"profilePic"] forState:UIControlStateNormal];
 
    }
    
    if (isArabic)
    {
        [self.btnUploadProp setBackgroundImage:[UIImage imageNamed:@"upload_property_app_but"] forState:UIControlStateNormal];
    }
    else
    {
        [self.btnUploadProp setBackgroundImage:[UIImage imageNamed:@"upload_but"] forState:UIControlStateNormal];
    }
    
    
    [tableView reloadData];
}
#pragma mark - contactListArray

-(void)contactListArray
{
    tableView.scrollEnabled=YES;
}



#pragma mark -tableview Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [contactListMA count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifair=@"MenuCustemCell";
    
    
    MenuCustemCell *cell=[self.tableView dequeueReusableCellWithIdentifier:cellIdentifair];
    
    if(cell==nil)
    {
        cell=[[MenuCustemCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifair];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    
    // cell.favouritImg.image = ICLocalizedImage([imagesMA objectAtIndex:indexPath.row]);
    cell.favouritImg.image = [UIImage imageNamed:[imagesMA objectAtIndex:indexPath.row]];
    
    //cell.backgroundColor=[UIColor colorWithPatternImage:cell.favouritImg.image];
    
    if(indexPath.row==2 || indexPath.row==3)
    {
        if([self isNotNull:[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN]])
        {
            [cell.favouritCountImg setHidden:NO];
            [cell.favouritCountLbl setHidden:NO];
            if(indexPath.row==2)
            {
                cell.favouritCountLbl.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_FAV_PROPERTY]];
            }
            else if (indexPath.row==3)
            {
                DLog(@"%@",[dataDict objectForKey:@"my_listed_property"]);
                cell.favouritCountLbl.text=[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:UD_LISTED_PROPERTY]];
            }
            cell.favouritCountLbl.text= LOCALIZATION(cell.favouritCountLbl.text);
        }
        else
        {
            [cell.favouritCountImg setHidden:YES];
            [cell.favouritCountLbl setHidden:YES];
        }
    }
    
    if(indexPath.row==6)
    {
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN]);
        if([self isNotNull:[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN]])
        {
            cell.favouritLabl.text=@"Logout";
            cell.favouritImg.image = [UIImage imageNamed:[imagesMA objectAtIndex:indexPath.row]];
        }
        else
        {
            cell.favouritLabl.text=[contactListMA objectAtIndex:indexPath.row];
            cell.favouritImg.image = [UIImage imageNamed:[imagesMA objectAtIndex:indexPath.row]];
        }
        
    }
    else
    {
        cell.favouritLabl.text=[contactListMA objectAtIndex:indexPath.row];
        cell.favouritImg.image = [UIImage imageNamed:[imagesMA objectAtIndex:indexPath.row]];
    }
    cell.favouritLabl.text= LOCALIZATION(cell.favouritLabl.text);
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0)
    {
        HomeVC *home  = ICLocalizedViewController([HomeVC class]);
        [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
        [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
        [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
        }];
    }
    if (indexPath.row==1) {
        AgentsListVC *agentList = ICLocalizedViewController([AgentsListVC class]);//[[AgentsListVC alloc]initWithNibName:@"AgentsListVC" bundle:nil];
        [(UINavigationController *)self.viewDeckController.centerController pushViewController:agentList animated:NO];
        [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:agentList]];
        [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
        }];
    }
    else if (indexPath.row==2)
    {
        if([self isNotNull:[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN]])
        {
            MyFavouritesVC *myFavourites = ICLocalizedViewController([MyFavouritesVC class]);//[[MyFavouritesVC alloc]initWithNibName:@"MyFavouritesVC" bundle:nil];
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:myFavourites animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:myFavourites]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
        }
        else
        {
            UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Baiti") message:LOCALIZATION(@"Please sign in")  delegate:self cancelButtonTitle:LOCALIZATION(@"Cancel") otherButtonTitles:LOCALIZATION(@"OK"), nil];
            [alrt show];
        }
    }
    else if (indexPath.row==3)
    {
        if([self isNotNull:[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN]])
        {
            MyListedPropertiesVC *myPropertiesList = ICLocalizedViewController([MyListedPropertiesVC class]);//[[MyListedPropertiesVC alloc]initWithNibName:@"MyListedPropertiesVC" bundle:nil];
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:myPropertiesList animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:myPropertiesList]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
        }
        else
        {
            UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Baiti") message:LOCALIZATION(@"Please sign in")  delegate:self cancelButtonTitle:LOCALIZATION(@"Cancel") otherButtonTitles:LOCALIZATION(@"OK"), nil];
            [alrt show];
        }
    }
    else if (indexPath.row==4)
    {
        if([self isNotNull:[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN]])
        {
            AdevertisementRequestVC *adertisementReq =ICLocalizedViewController([AdevertisementRequestVC class]);// [[AdevertisementRequestVC alloc]initWithNibName:@"AdevertisementRequestVC" bundle:nil];
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:adertisementReq animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:adertisementReq]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
        }
        else
        {
            UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Baiti") message:LOCALIZATION(@"Please sign in")  delegate:self cancelButtonTitle:LOCALIZATION(@"Cancel") otherButtonTitles:LOCALIZATION(@"OK"), nil];
            [alrt show];
        }
    }
    else if(indexPath.row==5)
    {
        if([self isNotNull:[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN]])
        {
            NotificationsVC *notifications = ICLocalizedViewController([NotificationsVC class]);//[[NotificationsVC alloc]initWithNibName:@"NotificationsVC" bundle:nil];
            [(UINavigationController *)self.viewDeckController.centerController pushViewController:notifications animated:NO];
            [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:notifications]];
            [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
            }];
            
        }
        else
        {
            UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Baiti") message:LOCALIZATION(@"Please sign in")  delegate:self cancelButtonTitle:LOCALIZATION(@"Cancel") otherButtonTitles:LOCALIZATION(@"OK"), nil];
            [alrt show];
        }
    }
    else
        if (indexPath.row==6)
        {
            
            
            if([self isNotNull:[[NSUserDefaults standardUserDefaults] valueForKey:UD_TOKEN]])
            {
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_TOKEN];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:UD_USERINFO];
                [[NSUserDefaults standardUserDefaults]synchronize];
                HomeVC *hvc = ICLocalizedViewController([HomeVC class]);//[[HomeVC alloc]initWithNibName:@"HomeVC" bundle:nil];
                [(UINavigationController *)self.viewDeckController.centerController pushViewController:hvc animated:NO];
                [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:hvc]];
                [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
                }];
                
  
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Sucessfully Logout")  withDelegate:self withTag:200];
                
                
            }
            else
            {
                SignInVC *signIn = ICLocalizedViewController([SignInVC class]);//[[SignInVC alloc]initWithNibName:@"SignInVC" bundle:nil];
                [(UINavigationController *)self.viewDeckController.centerController pushViewController:signIn animated:NO];
                [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:signIn]];
                [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
                }];
            }
        }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)uploadPropertyHere:(id)sender {
    DLog(@"-----TOKEN------- %@",[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN]);
    if([self isNotNull:[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN]])
    {
        PostProperty *post =  ICLocalizedViewController([PostProperty class]);//[[PostProperty alloc]initWithNibName:@"PostProperty" bundle:nil];
        [(UINavigationController *)self.viewDeckController.centerController pushViewController:post animated:NO];
        [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:post]];
        [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
        }];
    }
    else
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Baiti") message:LOCALIZATION(@"Please sign in")  delegate:self cancelButtonTitle:LOCALIZATION(@"Cancel") otherButtonTitles:LOCALIZATION(@"OK"), nil];
        [alrt show];
    }
    
}
- (IBAction)goToHome:(id)sender {
    //    HomeVC *home = [[HomeVC alloc]initWithNibName:@"HomeVC" bundle:nil];
    //    [(UINavigationController *)self.viewDeckController.centerController pushViewController:home animated:NO];
    //    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:home]];
    //    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    //    }];
    //
    
    HomeVC *resourceVC  = ICLocalizedViewController([HomeVC class]);
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:resourceVC animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:resourceVC]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];
}

- (IBAction)goToSettings:(id)sender {
    
    SettingsVC *profile =  ICLocalizedViewController([SettingsVC class]);//[[SettingsVC alloc]initWithNibName:@"SettingsVC" bundle:nil];
    [(UINavigationController *)self.viewDeckController.centerController pushViewController:profile animated:NO];
    [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:profile]];
    [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
    }];
}
- (IBAction)profileButtonClicked:(id)sender {
    
    if([self isNotNull:[[NSUserDefaults standardUserDefaults]valueForKey:UD_TOKEN]])
    {
        MyProfileVC *profile = ICLocalizedViewController([MyProfileVC class]);// [[MyProfileVC alloc]initWithNibName:@"MyProfileVC" bundle:nil];
        [(UINavigationController *)self.viewDeckController.centerController pushViewController:profile animated:NO];
        [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:profile]];
        [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
        }];
    }
    else
    {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Baiti") message:LOCALIZATION(@"Please sign in")  delegate:self cancelButtonTitle:LOCALIZATION(@"Cancel") otherButtonTitles:LOCALIZATION(@"OK"), nil];
        [alrt show];
    }
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        SignInVC *sivc = ICLocalizedViewController([SignInVC class]);//[[SignInVC alloc]initWithNibName:@"SignInVC" bundle:nil];
        [(UINavigationController *)self.viewDeckController.centerController pushViewController:sivc animated:NO];
        [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:sivc]];
        [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
        }];
    }
}
@end
