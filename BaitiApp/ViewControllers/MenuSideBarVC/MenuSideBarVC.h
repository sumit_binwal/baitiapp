//
//  MenuSideBarVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 27/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuSideBarVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *contactListMA;
     NSMutableArray *imagesMA;
    BOOL isArabic;
    

}
@property (strong, nonatomic) IBOutlet UIButton *btnUploadProp;


@property (strong, nonatomic) IBOutlet UIImageView *userprofilePic;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)profileButtonClicked:(id)sender;
- (IBAction)uploadPropertyHere:(id)sender;
- (IBAction)goToSettings:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *profileButtonClicked;





@property (strong, nonatomic)   NSMutableArray *contactListMA;
@property(nonatomic,strong)  NSMutableArray *imagesMA;

@end
