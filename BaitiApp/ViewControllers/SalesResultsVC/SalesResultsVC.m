//
//  SalesResultsVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 01/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "SalesResultsVC.h"


@interface SalesResultsVC ()

@end

@implementation SalesResultsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _arrayForPrices=[[NSMutableArray alloc]initWithObjects:@"$ 45,0000",@"$ 300000",@"$ 500000",@"$ 2400000",@"$ 8094595" ,nil];
    _arrayForNumOfRooms=[[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6", nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setupNavigationBar];
    [self setTitle];
    
}
#pragma mark-set NavigationBar

- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        
    }

        titleView.text =LOCALIZATION(@"Sales Results") ;

    
    [titleView sizeToFit];
}
-(void)setupNavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"list_icon"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    
    UIButton *favRightBtn1 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [favRightBtn1 setImage:[UIImage imageNamed:@"search_icon"] forState:UIControlStateNormal];
    [favRightBtn1 addTarget:self action:@selector(goToSearch:) forControlEvents:UIControlEventTouchUpInside];
    [favRightBtn1 setFrame:CGRectMake(22, 0, 32, 32)];
    
    UIButton *shareRightBtn2 =  [UIButton buttonWithType:UIButtonTypeCustom];
    [shareRightBtn2 setImage:[UIImage imageNamed:@"filter_icon"] forState:UIControlStateNormal];
    [shareRightBtn2 addTarget:self action:@selector(filter:) forControlEvents:UIControlEventTouchUpInside];
    [shareRightBtn2 setFrame:CGRectMake(55, 0, 32, 32)];
    
    UIView *rightBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 76, 32)];
    [rightBarButtonItems addSubview:favRightBtn1];
    [rightBarButtonItems addSubview:shareRightBtn2];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBarButtonItems];
    
}
-(void)goToSearch:(id)sender
{
    
    
}
-(void)filter:(id)sender
{
    [self.navigationController popViewControllerAnimated:TRUE];
}
#pragma mark -
#pragma mark - tableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _arrayForPrices.count;
    return _arrayForNumOfRooms.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SalesRsultsCustomTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SalesRsultsCustomTableViewCell"];
    
    if (cell==nil) {
        cell = [[SalesRsultsCustomTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SalesRsultsCustomTableViewCell"];
        
        
    }
    //    cell.labelForPrice.text=[self.arrayForPrices objectAtIndex:indexPath.row];
    //    cell.labelForNumOfRooms.text=[self.arrayForNumOfRooms objectAtIndex:indexPath.row];
    //    cell.labelForLocation.text=[self.arrayForLocations objectAtIndex:indexPath.row];
    //    cell.typesOfFlats=[self.arrayForTypeOfFlats objectAtIndex:indexPath.row];
    //    cell.BackGroundImageView.image=[self.arrayForBackGroundImages objectAtIndex:indexPath.row];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(SalesRsultsCustomTableViewCell* )cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CGFloat  height=192;
    return height;
}



@end
