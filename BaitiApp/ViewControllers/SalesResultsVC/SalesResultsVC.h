//
//  SalesResultsVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 01/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalesRsultsCustomTableViewCell.h"

@interface SalesResultsVC : UIViewController

@property NSMutableArray * arrayForPrices;
@property NSMutableArray * arrayForNumOfRooms;
@property NSMutableArray * arrayForLocations;
@property NSMutableArray * arrayForTypeOfFlats;
@property NSMutableArray * arrayForBackGroundImages;

@end
