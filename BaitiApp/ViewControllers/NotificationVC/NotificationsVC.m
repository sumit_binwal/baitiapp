//
//  NotificationsVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "NotificationsVC.h"
#import "DetailVC.h"

@interface NotificationsVC ()
{
    NSMutableArray *dataArr;
    NSString *propertyID;
    NSString *propertyOwnerID;
    IBOutlet UILabel *lblMsg;
}
@end

@implementation NotificationsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.notificationsTableView.contentInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    _userNamesArray =[[NSMutableArray alloc]initWithObjects:@"lie",@"anderson",@"jokey jon",@"scott", nil];
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
    
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setnavigationBar];
    [self setTitle];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getMyNotificationData];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - set navigationBar
- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        
    }

        titleView.text =LOCALIZATION(@"My Notifications") ;
    
    
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"list_icon"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
}
#pragma mark - Tableview Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArr.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NotificationsTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"NotificationsTableViewCell"];
    
    if (cell==nil) {
        cell = [[NotificationsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"NotificationsTableViewCell"];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
    }
    
    
    cell.userProfilePic.layer.masksToBounds = YES;
    cell.userProfilePic.layer.cornerRadius = cell.userProfilePic.bounds.size.width/2.0f;
    NSString *string00;
    NSString *string0;
    NSString *string1;
    NSString *text;

    string0 =LOCALIZATION(@"add favourites to") ;
        string00=[[dataArr objectAtIndex:indexPath.row] objectForKey:@"property_title"];
        string1=[[dataArr objectAtIndex:indexPath.row] objectForKey:@"user_fullname"];
        text = [NSString stringWithFormat:@"%@ %@ %@",
                          string00,string0 ,string1];


    
    //whole String attribute
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor colorWithRed:110.0f/255.0f green:110.0f/255.0f blue:110.0f/255.0f alpha:1],
                              NSFontAttributeName:[UIFont systemFontOfSize:14]
                              };
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
    
    NSRange string0Range = [text rangeOfString:string0];
    NSRange string1Range = [text rangeOfString:string1];
    NSRange string2Range =[text rangeOfString:string00];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor blackColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Bold" size:14.0f]}  range:string2Range];

    
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:239.0f/255.0f green:-2.0f/255.0f blue:125.0f/255.0f alpha:1],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Light" size:14.0f]}  range:string0Range];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:110.0f/255.0f green:110.0f/255.0f blue:110.0f/255.0f alpha:1],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Regular" size:14.0f]}  range:string1Range];
    
    [cell.lblFavouritesName setAttributedText:attributedText];
    

    cell.timeLabel.text=[[dataArr objectAtIndex:indexPath.row] objectForKey:@"time"];

    [cell.userProfilePic setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[dataArr objectAtIndex:indexPath.row] objectForKey:@"user_image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    propertyID=[[dataArr objectAtIndex:indexPath.row]objectForKey:@"propertyid"];
    propertyOwnerID=[[dataArr objectAtIndex:indexPath.row]objectForKey:@"property_owner_id"];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertyDetails];
    }
    else
    {
         [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
        
    }
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CGFloat  height=60;
    return height;
}

#pragma mark- WebService API
-(void)getMyNotificationData
{
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/usernotifications
    NSString *siteURL = @"usernotifications";
    
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",language_id,@"language_id",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            dataArr=[responseDict objectForKey:@"data"];
            [_notificationsTableView reloadData];
            
        }
        else
        {
            if ([[responseDict objectForKey:@"replyMsg"]isEqualToString:@"No notification found"]) {
                lblMsg.text=[responseDict objectForKey:@"replyMsg"];
            }
            else
            {
                if (isarabic)
                {
                      [CommonFunctions alertArabicTitle:@"" withMessage:@"لا يوجد معلومات" ];
                }
                else
                {
                    [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
                }
                            }
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}
-(void)getPropertyDetails
{
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/propertydetail
    NSString *siteURL = @"propertydetail";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"propertyid",propertyOwnerID,@"property_owner_id",language_id,@"language_id",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[[[responseDict objectForKey:@"data"]objectForKey:@"Property"]objectForKey:@"isFav"]  forKey:UD_FAVORITE];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            DetailVC *dtvc=ICLocalizedViewController([DetailVC class]);//[[DetailVC alloc]initWithNibName:@"DetailVC" bundle:nil];
            dtvc.detailDataDict=[responseDict objectForKey:@"data"];
            [self.navigationController pushViewController:dtvc animated:YES];
        }
        else
        {
            [[NSUserDefaults standardUserDefaults]setObject:[[[responseDict objectForKey:@"data"]objectForKey:@"Property"]objectForKey:@"isFav"]  forKey:UD_FAVORITE];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
            
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                  [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                               
                                          }
                                          
                                      }];
}

@end
