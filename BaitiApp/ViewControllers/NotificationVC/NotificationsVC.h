//
//  NotificationsVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationsTableViewCell.h"

@interface NotificationsVC : UIViewController
{
    BOOL isarabic;
}

@property (strong, nonatomic) IBOutlet UITableView *notificationsTableView;

@property NSMutableArray * userProfilePicsArray;
@property NSMutableArray * userNamesArray;

@end
