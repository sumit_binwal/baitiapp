//
//  EmailAgentVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 30/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailAgentVC : UIViewController<UIAlertViewDelegate>
{
    UITextField * textFld;
    
    BOOL isarabic;
}
@property (strong, nonatomic) NSString *strToEmail;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *viewForComponents;
@property (strong, nonatomic) IBOutlet UIImageView *imageForBuilderName;
@property (strong, nonatomic) IBOutlet UILabel *lblForPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblForRooms;
@property (strong, nonatomic) IBOutlet UILabel *lblForLocation;
@property (strong, nonatomic) IBOutlet UILabel *lblForSendToName;
@property (strong, nonatomic) IBOutlet UITextField *enterName;
@property (strong, nonatomic) IBOutlet UITextField *enterEmailHere;
@property (strong, nonatomic) IBOutlet UITextField *enterPhoneNum;
@property (strong, nonatomic) IBOutlet UITextView *enterMessageHere;
@property (strong,nonatomic)NSMutableArray *arrProperyData;
@property (strong,nonatomic)NSMutableDictionary *dataDict;

- (IBAction)clickToSendMail:(id)sender;

@end
