//
//  ForgotPasswordVC.m
//  BaitiApp
//
//  Created by VijayYadav on 02/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "ForgotPasswordVC.h"
#import "SignUpVC.h"
#import "IIViewDeckController.h"
@interface ForgotPasswordVC ()<UIAlertViewDelegate,UITextFieldDelegate>
{
    
    IBOutlet UIButton *btnForgotPass;
}
@end

@implementation ForgotPasswordVC
@synthesize msgDict;
@synthesize txtFldUserEmaiId,scrollview;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    msgDict=[[NSMutableDictionary alloc]init];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget: self action:@selector(doSingleTap)];
    singleTap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singleTap];
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
        imgtitle.image=[UIImage imageNamed:@"Signiv"];
    }
    else
    {
        isarabic=NO;
    }
 
    
    
if([UIScreen mainScreen].bounds.size.height<568)
{
    [scrollview setScrollEnabled:YES];
}
    else
    {
        [scrollview setScrollEnabled:NO];
    }
    // Do any additional setup after loading the view from its nib.
            [txtFldUserEmaiId setValue:[UIColor colorWithRed:74.0f/255.0f green:74.0f/255.0f blue:74.0f/255.0f alpha:1.0] forKeyPath:@"_placeholderLabel.textColor"];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self setButtonText];
}

//-(void)viewDidLayoutSubviews
//{
//    [scrollview setContentSize:CGSizeMake(320.0f, <#CGFloat height#>)]
//}


-(void)setButtonText
{
    NSString *string0;
    NSString *string1;
    NSString *text;
    if (isarabic)
    {
        string0 = @"إشارة";
        string1=@"كلمة السر";
        
        
        text = [NSString stringWithFormat:@"%@ %@",string0,string1];
    }
    else
    {
        string0 = @"Reset";
        string1=@"Password";
        
        text = [NSString stringWithFormat:@"%@ %@",string0,string1];
        
    }
    
    
    //whole String attribute
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor whiteColor],
                              NSFontAttributeName:[UIFont systemFontOfSize:14]
                              };
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
    
    NSRange string0Range = [text rangeOfString:string0];
    NSRange string1Range = [text rangeOfString:string1];
    
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Bold" size:18.0f]}  range:string0Range];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Light" size:18.0f]}  range:string1Range];
    
    [btnForgotPass setAttributedTitle:attributedText forState:UIControlStateNormal];
    
}


-(void)doSingleTap
{
    [self.view endEditing:YES];
    [scrollview setContentOffset:CGPointMake(0, 0) animated:YES];
    [scrollview setScrollEnabled:NO];
}
-(void)viewWillLayoutSubviews
{
    [scrollview setContentSize:CGSizeMake(320.0f, 600.0f)];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
}

-(IBAction)recoverPassword:(UIButton *)sender
{
    [self.view endEditing:YES];
    if ([CommonFunctions isValueNotEmpty:txtFldUserEmaiId.text])
    {
        if ([CommonFunctions IsValidEmail:txtFldUserEmaiId.text]) {
            if([CommonFunctions reachabiltyCheck])
            {
                [CommonFunctions showActivityIndicatorWithText:@""];
                [self UserRecoverPassword];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
            }

        }
        else
        {
                [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter valid email") ];
        }

    }
    else
    {

        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter email") ];
    }
}



#pragma mark - ScrollView

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    DLog(@"fsfjskfjdks;jf;sdjfkl;dsaj f");
    return YES;
}
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [scrollview setScrollEnabled:YES];
    [self scrollViewToCenterOfScreen:textField];
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    switch (textField.tag)
    {
       
        case 1:
            txtFldUserEmaiId.text=textField.text;
            [scrollview setContentOffset:CGPointMake(0, 0) animated:YES];
            [self.view endEditing:YES];
            
            break;
            
    }
    return YES;
}


- (void)scrollViewToCenterOfScreen:(UIView *)theView
{
    CGFloat viewCenterY = theView.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height -130.0f;
    
    CGFloat y = viewCenterY - avaliableHeight / 2.0f;
    
    if (y < 0)
    {
        y = 0;
    }
    
    [scrollview setContentOffset:CGPointMake(0, y)];
    [scrollview setContentSize:CGSizeMake(320.0f, 600.0f)];
}


-(void)showAlert: (NSDictionary *)alertData
{
    
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Email cannot be empty")  message:NSLocalizedString([alertData valueForKey:@"message"], nil) delegate:self cancelButtonTitle:LOCALIZATION(@"OK") otherButtonTitles:nil];
    [alert show];
    
}

-(void)OnSuccessMessage:(NSDictionary*)alertData
{
     
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:LOCALIZATION(@"Success") message:LOCALIZATION(@"You are now successfully registered with Baiti") delegate:self cancelButtonTitle:LOCALIZATION(@"OK") otherButtonTitles:nil];
    [alert show];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)UserRecoverPassword
{
    //http://192.168.0.170/baiti/mobile/forgotpassword
    NSString *siteURL = @"forgotpassword";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[CommonFunctions trimSpaceInString:txtFldUserEmaiId.text], @"email",language_id,@"language_id",nil];
    
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];

            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            if (isarabic)
            {
                [CommonFunctions alertTitleArabic:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil) withDelegate:self withTag:200];
            }
            else
            {
               [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] withDelegate:self withTag:200];
            }
            
            
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
            
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                        [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }

                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                   [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")]; 
                                          }
                                          
                                      }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==200)
    {
        IIViewDeckController *controller=[APPDELEGATE generateControllerStack];
        APPDELEGATE.window.rootViewController=controller;
    }
}
- (IBAction)signUpBtnClicked:(id)sender {
    SignInVC * signIn=ICLocalizedViewController([SignInVC class]);//[[SignUpVC alloc]initWithNibName:@"SignUpVC" bundle:nil];
    [self presentViewController:signIn animated:YES completion:nil];
}
- (IBAction)skipBtnClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
