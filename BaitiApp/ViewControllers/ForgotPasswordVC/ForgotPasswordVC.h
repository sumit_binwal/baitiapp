//
//  ForgotPasswordVC.h
//  BaitiApp
//
//  Created by VijayYadav on 02/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordVC : UIViewController<UIScrollViewDelegate>
{
    NSMutableDictionary *msgDict;
    BOOL isarabic;
    IBOutlet UIImageView *imgtitle;
}
@property(nonatomic,strong)   NSMutableDictionary *msgDict;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;
@property (strong, nonatomic) IBOutlet UITextField *txtFldUserEmaiId;
@end
