//
//  AgentsListVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAgentListTableViewCell.h"
#import "ConnectionManager.h"
@interface AgentsListVC : UIViewController
{
    NSMutableArray *arrAgentList;
    NSString *strPageNum;
     int totalPost;
    
    int currentPage;
    int totalPage;
    int apiCurrentPage;
    
    BOOL isarabic;
}

@property (strong, nonatomic) IBOutlet UITableView *agentListTableView;

@end
