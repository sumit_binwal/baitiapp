//
//  AgentsListVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "AgentsListVC.h"
#import "AgentDetailVC.h"

@interface AgentsListVC ()

@end

@implementation AgentsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.agentListTableView.contentInset = UIEdgeInsetsZero;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setnavigationBar];
    [self setTitle];
    
    strPageNum=@"1";
    totalPost=1;
    currentPage=1;
    arrAgentList=[[NSMutableArray alloc]init];
    
    [self getPropertyAgentListwithStrPage:[NSString stringWithFormat:@"%d",currentPage]];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - setUp NavigationBar
- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
    }
    
    titleView.text =LOCALIZATION(@"Agents") ;
    self.navigationItem.titleView = titleView;

    
    
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"list_icon"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    UIEdgeInsets imageInsets;
    
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-6.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
}
-(void)goToSearch:(id)sender
{
    
}

#pragma mark -
#pragma mark -tableview Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // return _arrayForPrices.count;
    return arrAgentList.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CustomAgentListTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"CustomAgentListTableViewCell"];
    
    if (cell==nil) {
        cell = [[CustomAgentListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CustomAgentListTableViewCell"];
        
        
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    
    DLog(@"%@",cell.lblPropertyListed.text);
    
    NSString *strNumberProperty=[NSString stringWithFormat:@"%@",[[arrAgentList objectAtIndex:indexPath.row] objectForKey:@"total_properties"] ];
    
    //Get Emoji Data
    NSData *data = [[[arrAgentList objectAtIndex:indexPath.row] objectForKey:@"username"]  dataUsingEncoding:NSUTF8StringEncoding];
    NSString *valueEmoj = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    cell.lblForAgentName.text=valueEmoj;
    
    // cell.imageForAgentName.image=[[arrAgentList objectAtIndex:indexPath.row] objectForKey:@"image"];
    cell.lblForLocatioName.text=NSLocalizedString([[arrAgentList objectAtIndex:indexPath.row] objectForKey:@"address"],nil);
    cell.noOfPropertiesListed.text=strNumberProperty;
    
    [cell.imageForAgentName sd_setImageWithURL:[[arrAgentList objectAtIndex:indexPath.row] objectForKey:@"image"]];
    
    
    
    if ((indexPath.row == [arrAgentList count]-1))
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        
        [self getPropertyAgentListwithStrPage:[NSString stringWithFormat:@"%d",currentPage]];
    }
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *strNumberProperty=[NSString stringWithFormat:@"%@",[[arrAgentList objectAtIndex:indexPath.row] objectForKey:@"total_properties"] ];
    if ([strNumberProperty isEqualToString:@"0"])
    {

        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"This agent dont has any property") ];
    }
    else
    {
        AgentDetailVC * agentList=ICLocalizedViewController([AgentDetailVC class]);//[[AgentDetailVC alloc]initWithNibName:@"AgentDetailVC" bundle:nil];
        agentList.strPageNumber= [NSString stringWithFormat:@"%d",apiCurrentPage];
        agentList.arrAgentDetails=[arrAgentList objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:agentList animated:YES];
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(CustomAgentListTableViewCell* )cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CGFloat  height=72;
    return height;
}

#pragma mark - WebService API
-(void)getPropertyAgentListwithStrPage:(NSString*)strPage
{
    if(totalPost==[arrAgentList count])
    {
        if (apiCurrentPage==totalPage)
        {
            [CommonFunctions removeActivityIndicator];
            return;
        }
    }
    
    NSMutableDictionary *params;
    params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:strPage,@"pagenumber",language_id,@"language_id",nil];
    
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/propertysearch
    NSString *siteURL = @"agentsearch";
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            
            apiCurrentPage=[[responseDict objectForKey:@"currentPage"] intValue];
            totalPage=[[responseDict objectForKey:@"totalPages"]intValue];
            totalPost=[[responseDict objectForKey:@"totalRecord"]intValue];
            
            if ([self isNotNull:[responseDict objectForKey:@"data"]])
            {
                
                
                
                NSMutableArray *tempArr = [[NSMutableArray alloc]init];
                totalPage = [[responseDict objectForKey:@"totalPages"] intValue];
                
                NSMutableArray *arr = [[responseDict objectForKey:@"data"] mutableCopy];
                
                for (int i = 0; i < [arr count]; i++)
                {
                    [tempArr addObject:[arr objectAtIndex:i]];
                    
                }
                
                
                if (currentPage == 1)
                {
                    [arrAgentList removeAllObjects];
                    
                }
                
                for (int i = 0; i < [tempArr count]; i++)
                {
                    [arrAgentList addObject:[tempArr objectAtIndex:i]];
                }
                
                [tempArr removeAllObjects];
                
                tempArr = nil;
                
                DLog(@"user info Arr :%@",arrAgentList);
                
                currentPage++;
                
                // isLoading = NO;
                //[self.agentListTableView setHidden:YES];
                
                [self.agentListTableView reloadData];
            }
            else
            {
                
                if ([[responseDict objectForKey:@"replyMsg"]isEqualToString:@"No result found"]) {
                    [arrAgentList removeAllObjects];
                    [self.agentListTableView reloadData];
                    
                }
                else
                {
                    if (isarabic)
                    {
                        [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
                    }
                    else
                    {
                        [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
                    }
                    
                    
                    
                }
                
            }
            
            
        }
        
    }withFailure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if([operation.response statusCode]  == 400 ){
             DLog(@"impo%@",operation.response);
             
             [CommonFunctions removeActivityIndicator];
             if (isarabic)
             {
                 [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
             }
             else
             {
                 [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
             }
             
             
             
         }
         else{
             
             [CommonFunctions removeActivityIndicator];
              [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];             
             
             
         }
         
     }];
    
    
}




@end
