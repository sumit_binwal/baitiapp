//
//  AdvertisementTypeVC.h
//  BaitiApp
//
//  Created by Sumit Sharma on 25/06/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PropertySelectedDelegate <NSObject>

-(void)setPropertyResults:(NSMutableArray*)datatDict withCompleteResult:(NSMutableArray*)completeResult;
@end

@interface AdvertisementTypeVC : UIViewController
{
    NSMutableArray *arrPreviousSelected;
    
    BOOL haveData;
    
    
}
@property (strong, nonatomic) NSMutableArray *cellSelected;
@property (strong, nonatomic) NSMutableArray *selectedArr;

@property (assign, nonatomic) id<PropertySelectedDelegate> propertyDelegate;
@end
