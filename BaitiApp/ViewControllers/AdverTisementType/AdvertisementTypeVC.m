//
//  AdvertisementTypeVC.m
//  BaitiApp
//
//  Created by Sumit Sharma on 25/06/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "AdvertisementTypeVC.h"
#import "AdevertisementRequestVC.h"
@interface AdvertisementTypeVC ()<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *tableVw;
    NSMutableArray *arrtableContent;
    IBOutlet UIButton *doneButtonClicked;
    IBOutlet UILabel *lblMsg;
}
@end

@implementation AdvertisementTypeVC
@synthesize propertyDelegate,selectedArr;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.cellSelected = [NSMutableArray array];
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
        [self getPropertytypes];
    }
    else
    {
    [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
- (IBAction)doneButtonClicked:(id)sender {
    
    if (self.cellSelected.count>0) {
        if([propertyDelegate respondsToSelector:@selector(setPropertyResults:withCompleteResult:)])
        {
            [propertyDelegate setPropertyResults:self.cellSelected withCompleteResult:arrtableContent];
        }
        
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    else
    {
        if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
        {
            
        [CommonFunctions alertArabicTitle:@"" withMessage:@"عنوان أختار نوع العقار"];
            
        }
        [CommonFunctions alertTitle:@"" withMessage:@"Please select atleast one property type first..."];
    }
}
- (IBAction)cancelButtonClicked:(id)sender
{
    
    
    if (arrPreviousSelected.count>0)
    {
        [self.cellSelected addObjectsFromArray:arrPreviousSelected];
        DLog(@"%@",self.cellSelected);
    }
    [tableVw reloadData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark- tableviewDelegates
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.0f;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrtableContent count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    if ([self.cellSelected containsObject:indexPath])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    
    cell.textLabel.text = [[[arrtableContent objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"title"];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //if you want only one cell to be selected use a local NSIndexPath property instead of array. and use the code below
    //self.selectedIndexPath = indexPath;
    
    //the below code will allow multiple selection
    if ([self.cellSelected containsObject:indexPath])
    {
        [self.cellSelected removeObject:indexPath];
        if (arrPreviousSelected.count==0)
        {
            arrPreviousSelected=[[NSMutableArray alloc]init];
        }
        
        
        if (haveData)
        {
            [ arrPreviousSelected addObject:indexPath];
        }
    }
    else
    {
        [self.cellSelected addObject:indexPath];
        if (haveData)
        {
            [ arrPreviousSelected removeObject:indexPath];
        }
    }
    DLog(@"%@",arrPreviousSelected);
    
    [tableView reloadData];
}

#pragma mark- get Property Names
-(void)getPropertytypes
{
    NSString *siteURL = @"userproperties";
    
    //  NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"propertyid",_addNotesTextView.text,@"description",@"1",@"noteid",nil];
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
       
                [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
           
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            arrtableContent=[[NSMutableArray alloc]init];
            arrtableContent=[responseDict objectForKey:@"data"];
            if (arrtableContent.count>0)
            {
                if (selectedArr.count>0) {
                    self.cellSelected=selectedArr;
                    haveData=YES;
                }
                [tableVw reloadData];
            }
            else
            {
       
                lblMsg.text=LOCALIZATION(@"No data found!!");
         
                [tableVw setHidden:YES];
                [doneButtonClicked setHidden:YES];
            }
            //[CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] withDelegate:self withTag:100];
        }
        else
        {
            
            if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }

        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              [CommonFunctions removeActivityIndicator];
                                              if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }

                                          }
                                          else{
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
                                          }
                                      }];
}
@end
