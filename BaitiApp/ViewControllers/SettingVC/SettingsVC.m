//
//  SettingsVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 14/05/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "SettingsVC.h"
#import "ViewContrTableCell.h"
#import "SettingViewVC.h"
#import "MenuSideBarVC.h"
#import "HomeVC.h"

@interface SettingsVC ()
{
    BOOL isArabic;
}
@end

@implementation SettingsVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden=NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"----------------------%@",language_id);
    [self setnavigationBar];
    [self setTitle];
    self.navigationController.navigationBar.titleTextAttributes=@{UITextAttributeTextColor : [UIColor whiteColor]};
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];

    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isArabic=YES;
    }
    else
    {
        isArabic=NO;
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of rany resources that can be recreated.
}

#pragma mark-setNavigationbar

- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
       
        
    }

    array=[[NSMutableArray alloc]initWithObjects:@"About Us",@"Terms and Conditions",@"Contact us",@"FAQ",nil];
    
    titleView.text = LOCALIZATION(@"Settings");
        self.navigationItem.titleView = titleView;
    
    [titleView sizeToFit];
}

-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor whiteColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"list_icon"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    UIEdgeInsets imageInsets;
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-8.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
}
#pragma mark - tableViewDelegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 4;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    ViewContrTableCell *cell=ICLocalizedViewController(ViewContrTableCell class)
    ViewContrTableCell *cell=[tableView dequeueReusableCellWithIdentifier:@"ViewContrTableCell"];
    
    if (cell==nil) {
        cell = [[ViewContrTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ViewContrTableCell"];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        
    }
    
    UIImageView * imagView = [[UIImageView alloc]init];
    imagView.backgroundColor=[UIColor grayColor];
    imagView.frame = CGRectMake(10,cell.contentView.frame.size.height - 1.0, self.view.frame.size.width-20, 1);
    [cell.contentView addSubview:imagView];
    
    cell.label.text=LOCALIZATION([array objectAtIndex:indexPath.row]);
    
    NSString *lang = [[NSUserDefaults standardUserDefaults]
                      objectForKey:@"ICPreferredLanguage"];
    if ([lang isEqualToString:@"ar"])
    {
        cell.label.textAlignment=NSTextAlignmentRight;
    }
    else
    {
        cell.label.textAlignment=NSTextAlignmentLeft;
    }
    
    return cell;
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SettingViewVC *svc=ICLocalizedViewController([SettingViewVC class]);//[[SettingViewVC alloc]initWithNibName:@"SettingViewVC" bundle:nil];
    
    svc.strNavTitle=LOCALIZATION([array objectAtIndex:indexPath.row]);
    
    if (indexPath.row==0)
    {
        if (isArabic) {
        svc.strWbUrl=@"http://mymeetingdesk.com/mobile/baiti/nodes/about_us/ar";
        }
        else
        {
        svc.strWbUrl=@"http://mymeetingdesk.com/mobile/baiti/nodes/about_us/en";
        }

        // svc.strNavTitle=@"About Us";
        [self.navigationController pushViewController:svc animated:YES];
    }
    else if (indexPath.row==1)
    {
        if (isArabic) {
            svc.strWbUrl=@"http://mymeetingdesk.com/mobile/baiti/nodes/terms_conditions/ar";
        }
        else
        {
            svc.strWbUrl=@"http://mymeetingdesk.com/mobile/baiti/nodes/terms_conditions/en";
        }
         //svc.strNavTitle=@"T&C";
        [self.navigationController pushViewController:svc animated:YES];
    }
    else if (indexPath.row==2)
    {
        if (isArabic) {
            svc.strWbUrl=@"http://mymeetingdesk.com/mobile/baiti/nodes/contact_us/ar";
        }
        else
        {
            svc.strWbUrl=@"http://mymeetingdesk.com/mobile/baiti/nodes/contact_us/en";
        }

        // svc.strNavTitle=@"Contact Us";
        [self.navigationController pushViewController:svc animated:YES];
    }
    else if (indexPath.row==3)
    {
        if (isArabic) {
            svc.strWbUrl=@"http://mymeetingdesk.com/mobile/baiti/nodes/faq/ar";
        }
        else
        {
            svc.strWbUrl=@"http://mymeetingdesk.com/mobile/baiti/nodes/faq/en";
        }
        // svc.strNavTitle=@"FAQ";
        [self.navigationController pushViewController:svc animated:YES];
    }
    
    
}

-(IBAction)englishButtonClicked:(id)sender
{
    
    //    [Localization sharedInstance].fallbackLanguage = @"ar";
    //    [Localization sharedInstance].preferredLanguage = @"en";
    
    
    
    [[LocalizationSystem shared]setLanguage:@"en"];
    DLog(@"pressed english");
    language_id=@"en";
    [[Localisator sharedInstance]setNewLanguage:kEnglish];
    IIViewDeckController* deckController = [self generateControllerStack];
    
    [APPDELEGATE.window setRootViewController:deckController];
}

-(IBAction)arabicButtonClicked:(id)sender
{
    [[Localisator sharedInstance]setNewLanguage:kArabic];
    [[LocalizationSystem shared]setLanguage:@"ar"];
    DLog(@"pressed english");
        language_id=@"ar";
    IIViewDeckController* deckController = [self generateControllerStack];
    
    [APPDELEGATE.window setRootViewController:deckController];
}





- (IIViewDeckController*)generateControllerStack
{
    MenuSideBarVC *rearViewController = ICLocalizedViewController([MenuSideBarVC class]);//[[MenuSideBarVC alloc]init];
    UIViewController *centerController;
    //    if (remoteNotification)
    //    {
    //        centerController = [[NotificationsVC alloc]initWithNibName:@"NotificationsVC" bundle:nil];
    //    }
    //    else
    //    {
    centerController = ICLocalizedViewController([SettingsVC class]);//[[HomeVC alloc]initWithNibName:@"HomeVC" bundle:nil];
    // }
    
    
    centerController = [[UINavigationController alloc] initWithRootViewController:centerController];
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:centerController leftViewController:rearViewController rightViewController:nil];
    deckController.rightSize = 100;
    [deckController disablePanOverViewsOfClass:NSClassFromString(@"_UITableViewHeaderFooterContentView")];
    return deckController;
}

@end
