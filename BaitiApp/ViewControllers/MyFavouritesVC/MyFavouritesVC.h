//
//  MyFavouritesVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 01/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyFavCustomTableViewCell.h"
#import "MGSwipeButton.h"
#import "MGSwipeTableCell.h" 

@interface MyFavouritesVC : UIViewController<UIGestureRecognizerDelegate,MGSwipeTableCellDelegate>
{
    BOOL isarabic;
}

@property (strong, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)ForsaleAndRent:(id)sender;

@property NSMutableArray * arrayForBuilderImages;
@property NSMutableArray * arrayForPrices;
@property NSMutableArray *  arrayForNumOfRooms;
@property NSMutableArray * arrayForLocations;
@property NSMutableArray * arrayForTypeOfFlats;


@end
