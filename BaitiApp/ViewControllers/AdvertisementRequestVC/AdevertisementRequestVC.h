//
//  AdevertisementRequestVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface AdevertisementRequestVC : UIViewController
{
    BOOL countFlip;
    NSMutableArray *arrtableContent;
    
    __weak IBOutlet UITableView *tablepropertyType;
    
    __weak IBOutlet UIButton *btnSubmit;
    
    BOOL submitPressed;
    
    BOOL isarabic;
}

@property (strong, nonatomic) IBOutlet UITextField *selectProperty;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *ViewForComponents;
@property (strong, nonatomic) IBOutlet UIButton *featureListingBtn;
@property (strong, nonatomic) IBOutlet UIButton *bannerAdsBtn;
@property (strong, nonatomic) IBOutlet UITextField *enterFirstName;
@property (strong, nonatomic) IBOutlet UITextField *enterLastName;
@property (strong, nonatomic) IBOutlet UITextField *enterEmailAddress;
@property (strong, nonatomic) IBOutlet UITextField *enterCompanyName;
@property (strong, nonatomic) IBOutlet UITextField *enterPhoneNum;


@property (strong, nonatomic) NSMutableArray *cellSelected;

- (IBAction)forFeatureListingBtn:(id)sender;
- (IBAction)forBannerAds:(id)sender;
- (IBAction)submitAdRequestHere:(id)sender;


@end
