//
//  AdevertisementRequestVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "AdevertisementRequestVC.h"
#import "AdvertisementTypeVC.h"
#import "HomeVC.h"

@interface AdevertisementRequestVC ()<UIAlertViewDelegate,UITextFieldDelegate,PropertySelectedDelegate>

{
    IBOutlet UIView *toolbarView;
    IBOutlet UILabel *lblMsg;
    UITextField *activetf;
    IBOutlet UIView *viewTable;
}
@end

@implementation AdevertisementRequestVC

bool isShown = false;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.cellSelected = [NSMutableArray array];
    
    self.scrollView.contentSize=_ViewForComponents.frame.size;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setUpView];
    
    [viewTable setFrame:CGRectMake(-500.0f, _selectProperty.frame.origin.y-5,[UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height/2+80)];
    
    _enterPhoneNum.inputAccessoryView=toolbarView;
    
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
    
    
    
    
}

-(void)dismisstableview
{
    //[self textFieldShouldBeginEditing:_enterFirstName];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setnavigationBar];
    [self setTitle];
    DLog(@"%@",_selectProperty.text);
    //[self dismisstableview];
    
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
}

#pragma mark- Filter results
-(void)setPropertyResults:(NSMutableArray *)datatDict withCompleteResult:(NSMutableArray *)completeResult
{
    self.cellSelected=datatDict;
    arrtableContent=completeResult;
    DLog(@"%@",datatDict);
    NSString *uploadString = [datatDict  componentsJoinedByString:@","];
    for (int i=0; i<datatDict.count; i++)
    {
        NSIndexPath *index=[datatDict objectAtIndex:i];
        if (i==0)
        {
            uploadString=[NSString stringWithFormat:@"%@",[[[completeResult objectAtIndex:index.row] objectForKey:@"Property"] objectForKey:@"title"]];
        }
        else
        {
            uploadString=[uploadString stringByAppendingString:[NSString stringWithFormat:@",%@",[[[completeResult objectAtIndex:index.row] objectForKey:@"Property"] objectForKey:@"title"]]];
        }
        
    }
    
    DLog(@"%@",uploadString);
    _selectProperty.text=uploadString;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - setUp navigationBar
- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView)
    {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
    }
    
    titleView.text =LOCALIZATION(@"Advertisement Request");
    self.navigationItem.titleView = titleView;
    
        [titleView sizeToFit];
}
-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    
    [leftButton setImage:[UIImage imageNamed:@"list_icon"] forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    UIEdgeInsets imageInsets;
    
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-6.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
}
-(void)setUpView
{
    UIView *v1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterFirstName.leftViewMode=UITextFieldViewModeAlways;
    _enterFirstName.leftView=v1;
    
    UIView *v2=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterLastName.leftViewMode=UITextFieldViewModeAlways;
    _enterLastName.leftView=v2;
    
    UIView *v3=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterEmailAddress.leftViewMode=UITextFieldViewModeAlways;
    _enterEmailAddress.leftView=v3;
    
    UIView *v4=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterCompanyName.leftViewMode=UITextFieldViewModeAlways;
    _enterCompanyName.leftView=v4;
    
    
    UIView *v5=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterPhoneNum.leftViewMode=UITextFieldViewModeAlways;
    _enterPhoneNum.leftView=v5;
    
    UIView *v6=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _selectProperty.leftViewMode=UITextFieldViewModeAlways;
    _selectProperty.leftView=v6;
    
    
}

-(void)myMethod{
    SWRevealViewController *reveal = self.revealViewController;
    [reveal revealToggleAnimated:YES];
}
- (IBAction)forFeatureListingBtn:(id)sender {
    
    if ([sender isSelected]) {
        [_featureListingBtn setBackgroundImage:[UIImage imageNamed:@"radio_buttion_selected"] forState:UIControlStateSelected];
        [sender setSelected:YES];
        [_bannerAdsBtn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateSelected];
    }
    
}

- (IBAction)forBannerAds:(id)sender {
    
    if ([sender isSelected]) {
        [_bannerAdsBtn setBackgroundImage:[UIImage imageNamed:@"radio_buttion_selected"] forState:UIControlStateSelected];
        [sender setSelected:YES];
        [_featureListingBtn setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateSelected];
    }
    
}
#pragma mark -textfield Delegates

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField ==_enterLastName||textField==_enterFirstName||textField==_enterEmailAddress||textField==_enterCompanyName)
    {
        if ([textField.text length]<20)
        {
            return YES;
        }
        else
        {
            
            if (textField.text.length>20)
            {
                for (int i=textField.text.length; i>19;i-- )
                {
                    if (i==19)
                    {
                        break;
                    }
                    else
                    {
                        textField.text = [textField.text substringToIndex:[textField.text  length] - 1];
                    }
                }
            }
            
            return YES;
            
        }
        
        
        
        
    }
    else if (textField==_enterPhoneNum)
    {
        if ([textField.text length]<11)
        {
            return YES;
        }
        else
        {
            
            if (textField.text.length>11)
            {
                for (int i=textField.text.length; i>10;i-- )
                {
                    if (i==10)
                    {
                        break;
                    }
                    else
                    {
                        textField.text = [textField.text substringToIndex:[textField.text  length] - 1];
                    }
                }
            }
            
            return YES;
            
        }
    }
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (textField==_selectProperty)
    {
        [self.view endEditing:YES];
        AdvertisementTypeVC *adtvc=ICLocalizedViewController([AdvertisementTypeVC class]);
        adtvc.propertyDelegate=self;
        DLog(@"%@",self.cellSelected);
        if (self.cellSelected.count>0) {
            
            adtvc.selectedArr=self.cellSelected;
        }
        [self presentViewController:adtvc animated:YES completion:nil];
    }
    
}

-(void)animationView
{
    [UIView animateWithDuration:01.0f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionFlipFromLeft |UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         [btnSubmit setFrame:CGRectMake(btnSubmit.frame.origin.x+500, btnSubmit.frame.origin.y, btnSubmit.frame.size.width, btnSubmit.frame.size.height)];
                         
                         [viewTable setFrame:CGRectMake(0, _selectProperty.frame.origin.y-5, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height/2+80)];
                     }
                     completion:nil];
    
    
}

-(void)animationViewLeft
{
    countFlip=YES;
    
    [viewTable setFrame:CGRectMake(0, _selectProperty.frame.origin.y-5, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height/2+80)];
    
    [UIView animateWithDuration:01.0f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionFlipFromRight
                     animations:^{
                         
                         [viewTable setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width+[UIScreen mainScreen].bounds.origin.x, _selectProperty.frame.origin.y-5, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height/2+80)];
                         
                         [btnSubmit setFrame:CGRectMake(btnSubmit.frame.origin.x-500, btnSubmit.frame.origin.y, btnSubmit.frame.size.width, btnSubmit.frame.size.height)];
                     }
                     completion:nil];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==_selectProperty) {
        
        [self.enterFirstName becomeFirstResponder];
        
    }
    else if (textField==_enterFirstName) {
        
        [self.enterLastName becomeFirstResponder];
    }
    else if(textField==_enterLastName){
        
        [self.enterEmailAddress becomeFirstResponder];
    }
    else if(textField==_enterEmailAddress)
    {
        [self.enterCompanyName becomeFirstResponder];
    }
    else if (textField==_enterCompanyName)
    {
        [self.enterPhoneNum becomeFirstResponder];
    }
    else if (textField==_enterPhoneNum)
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}
#pragma mark-textField Validations
-(BOOL)textFieldValidations
{
    BOOL success=NO;
    
    if(![CommonFunctions isValueNotEmpty:_selectProperty.text])
    {
        
        [_selectProperty becomeFirstResponder];
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please select at least one property")  withDelegate:self];
        return success;
        
    }
    else if(![CommonFunctions isValueNotEmpty:_enterFirstName.text])
    {
        
        [_enterFirstName becomeFirstResponder];
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter username")  withDelegate:self];
        return success;
        
    }
    else if (![CommonFunctions isValueNotEmpty:_enterLastName.text])
    {
        
        [_enterLastName  becomeFirstResponder];
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter fullname")   withDelegate:self];
        return success;
        
    }
    else if (![CommonFunctions isValueNotEmpty:_enterEmailAddress.text])
    {
        
        [_enterEmailAddress becomeFirstResponder];
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter email address")  withDelegate:self];
        return success;
        
    }
    else if (![CommonFunctions IsValidEmail:_enterEmailAddress.text])
    {
        
        [_enterEmailAddress becomeFirstResponder];
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter valid email address")  withDelegate:self];
        return success;
        
    }
    else if (![CommonFunctions isValueNotEmpty:_enterCompanyName.text])
    {
        
        [_enterCompanyName becomeFirstResponder];
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter company name")  withDelegate:self];
        return success;
    }
    else if (![CommonFunctions isValueNotEmpty:_enterPhoneNum.text])
    {
        activetf=_enterPhoneNum;
        [_enterPhoneNum becomeFirstResponder];
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter phone number")  withDelegate:self];
        return success;
    }
    else if (_enterPhoneNum.text.length<10)
    {
        activetf=_enterPhoneNum;
        [_enterPhoneNum becomeFirstResponder];
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter 10 digit phone number")  withDelegate:self];
        return success;
    }
    
    return success=YES;
}
- (IBAction)submitAdRequestHere:(id)sender {
    [self.view endEditing:YES];
    if (_selectProperty.text.length>1)
    {
        if ([self textFieldValidations])
        {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self sendPropertyRequests];
        }
    }
    else
    {

        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please select at least one property")];
        
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==1000)
    {
        HomeVC *home=[[HomeVC alloc]initWithNibName:@"HomeVC" bundle:nil];
        [self.navigationController pushViewController:home animated:YES];
    }
    else if (activetf==_selectProperty)
    {
        [_selectProperty becomeFirstResponder];
    }
    else if (activetf==_enterFirstName) {
        [_enterFirstName becomeFirstResponder];
    }
    else if (activetf==_enterLastName)
    {
        [_enterLastName becomeFirstResponder];
    }
    else if (activetf==_enterEmailAddress)
    {
        [_enterEmailAddress becomeFirstResponder];
    }
    else if (activetf==_enterCompanyName)
    {
        [_enterCompanyName becomeFirstResponder];
    }
    else if (activetf==_enterPhoneNum)
    {
        [_enterPhoneNum becomeFirstResponder];
    }
}
#pragma mark- WebService API
-(void)saveNotes
{
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/addnote
    NSString *siteURL = @"addnote";
    //NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"propertyid",_addNotesTextView.text,@"description",@"1",@"noteid",nil];
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",language_id,@"language_id",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            if (isarabic)
            {
                [CommonFunctions alertTitleArabic:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg" ],nil) withDelegate:self withTag:100];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"] withDelegate:self withTag:100];
            }
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
        }
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                              
                                          }
                                          
                                      }];
}


#pragma mark- send Property Requests
-(void)sendPropertyRequests
{
    NSString *siteURL = @"addrequest";
    
    
    NSString *strPropertIds;
    for (int i=0; i<self.cellSelected.count; i++)
    {
        NSIndexPath *index=[self.cellSelected objectAtIndex:i];
        if (i==0)
        {
            strPropertIds=[[[arrtableContent objectAtIndex:index.row] objectForKey:@"Property"] objectForKey:@"id"];
        }
        else
        {
            strPropertIds=[strPropertIds stringByAppendingString:[NSString stringWithFormat:@",%@",[[[arrtableContent objectAtIndex:index.row] objectForKey:@"Propertytype"] objectForKey:@"id"]]];
        }
        
    }

    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:strPropertIds, @"property_ids",_enterFirstName.text,@"username",_enterLastName.text,@"fullname",_enterEmailAddress.text,@"email",_enterCompanyName.text,@"companyname",_enterPhoneNum.text,@"phonenumber",language_id,@"language_id",nil];
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",language_id,@"language_id",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
            
            HomeVC *home=ICLocalizedViewController([HomeVC class]);//[[HomeVC alloc]initWithNibName:@"HomeVC" bundle:nil];
            
            [self.navigationController pushViewController:home animated:YES];
            
        }
        else
        {
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
            
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                          }
                                          
                                      }];
    
}
- (IBAction)cancelButtonClicked:(id)sender {
    [_enterPhoneNum setText:@""];
    [_enterPhoneNum resignFirstResponder];
}
- (IBAction)doneButtonClicked:(id)sender {
    [_enterPhoneNum resignFirstResponder];
}

@end
