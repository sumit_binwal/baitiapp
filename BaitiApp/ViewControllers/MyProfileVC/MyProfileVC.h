//
//  MyProfileVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 25/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileVC : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) IBOutlet UIImageView *userProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *nameOfTheUser;
@property (strong, nonatomic) IBOutlet UILabel *locationOfTheUser;
@property (strong, nonatomic) IBOutlet UILabel *email;
@property (strong, nonatomic) IBOutlet UILabel *mobileNum;

- (IBAction)settingsBtnAction:(id)sender;
- (IBAction)goToEditVC:(id)sender;


@end
