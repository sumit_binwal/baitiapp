//
//  SignUpVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 25/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignInVC.h"

@interface SignUpVC : UIViewController<UIScrollViewDelegate>
{
    IBOutlet UIImageView *imgSignUP;
    BOOL check;
  NSMutableDictionary *msgDict;
    BOOL isarabic;
}
@property (strong, nonatomic) IBOutlet UITextField *txtFldUserName;
@property (strong, nonatomic) IBOutlet UITextField *txtFldUserEmaiId;
@property (strong, nonatomic) IBOutlet UITextField *txtFldUsePassword;
@property (strong, nonatomic) IBOutlet UITextField *txtFldUseConfrimPassword;
@property (strong, nonatomic) IBOutlet UIButton *subScribeBtn;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;


- (IBAction)subscribeBtnAction:(id)sender;

- (IBAction)signInHere:(id)sender;


@property (strong, nonatomic) NSMutableDictionary *msgDict;

@end
