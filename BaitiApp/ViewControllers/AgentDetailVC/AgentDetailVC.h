//
//  AgentDetailVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyListedPropertyCell.h"
#import "DetailVC.h"

@interface AgentDetailVC : UIViewController
{
    BOOL isarabic;
}

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong, nonatomic) IBOutlet UIImageView *agentDetailpic;

@property (strong, nonatomic) IBOutlet UILabel *nameOfTheProperty;
@property (strong, nonatomic) IBOutlet UILabel *nameOftheArea;
@property (strong, nonatomic) IBOutlet UILabel *noOfPropertiesListed;
@property (strong, nonatomic) IBOutlet UITextView *descOfProperty;
@property (strong, nonatomic) IBOutlet UIButton *propertiesListedBy;
@property (strong, nonatomic) IBOutlet UITableView *AgentDetailTableView;

@property (strong, nonatomic) NSMutableArray *arrAgentDetails;
@property (strong, nonatomic) NSMutableArray *arrPropertyData;
@property (strong, nonatomic) NSMutableArray *arrAgentPropertyDetails;
@property (strong, nonatomic) NSString *strPageNumber;
- (IBAction)clickToMailHere:(id)sender;
- (IBAction)clickToCallHere:(id)sender;


@end
