//
//  AgentDetailVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "AgentDetailVC.h"
#import "EmailAgentVC.h"

@interface AgentDetailVC ()
{
    int currentPage;
    int totalRecord;
    IBOutlet NSLayoutConstraint *lblDiscriptionHeightConstratint;
    int totalPage;
    int apiCurrentpage;
    
    IBOutlet UILabel *lblDiscription;
    IBOutlet UILabel *lblMsg;
}
@end

@implementation AgentDetailVC

@synthesize arrAgentDetails,strPageNumber,arrPropertyData,arrAgentPropertyDetails;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout=UIRectEdgeNone;
    
    _agentDetailpic.layer.cornerRadius = _agentDetailpic.frame.size.width / 2;
    _agentDetailpic.clipsToBounds = YES;
    _agentDetailpic.layer.borderColor=[UIColor colorWithRed:(109/255.0f) green:(109/255.0f) blue:(109/255.0f) alpha:1.0f].CGColor;
    _agentDetailpic.layer.borderWidth=2.0f;
    arrPropertyData=[[NSMutableArray alloc]init];
    
    totalRecord=1;
    currentPage=1;
    NSString *strAgentID=[arrAgentDetails valueForKey:@"id"];
    [self getAgentDetailwithStrPage:[NSString stringWithFormat:@"%d",currentPage] withAgetntId:strAgentID];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
    [self.view endEditing:YES];
    
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setnavigationBar];
    [self setTitle];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - setNavigationBar
- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
    }
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }
    
    titleView.text =LOCALIZATION(@"Agent Details") ;
    self.navigationItem.titleView = titleView;
    
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    UIEdgeInsets imageInsets;
    
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-6.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
}

#pragma mark-userDetails

-(void)setUserDetails
{
    [self.agentDetailpic sd_setImageWithURL:[[arrAgentDetails objectAtIndex:0]valueForKey:@"image"]];
    
    NSData *data = [[[arrAgentDetails objectAtIndex:0] valueForKey:@"username"]  dataUsingEncoding:NSUTF8StringEncoding];
    
    
    NSString *valueEmoj = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
    self.nameOfTheProperty.text=valueEmoj;
    
    
    [self.noOfPropertiesListed setText:[NSString stringWithFormat:@"%@",[[arrAgentDetails objectAtIndex:0] valueForKey:@"total_properties"]]];
    [self.nameOftheArea setText:[NSString stringWithFormat:@"%@",[[arrAgentDetails objectAtIndex:0] valueForKey:@"address"]]] ;
    NSString *strDescription=[NSString stringWithFormat:@"%@",[[arrAgentDetails objectAtIndex:0] valueForKey:@"short_description"]];
    
    if ( ![self isNotNull:[[arrAgentDetails objectAtIndex:0] valueForKey:@"short_description"]])
        
        
        
        
    {
        lblDiscription.text=@"No Discription";
        
        lblDiscriptionHeightConstratint.constant=20;

    }
    else
    {
        
        
        
        if (strDescription.length<1) {
        [lblDiscription setText:@"No Discription"];
            lblDiscriptionHeightConstratint.constant=20;
        }
        else
        {
            [lblDiscription setText:strDescription];
        }
    }
    
}

-(void)goBack:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -tableview Delegates

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrPropertyData.count;
    //return 1;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyListedPropertyCell *cell=[tableView dequeueReusableCellWithIdentifier:@"MyListedPropertyCell"];
    
    if (cell==nil) {
        cell = [[MyListedPropertyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MyListedPropertyCell"];
        
        
    }
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    NSString *strNumberProperty=[NSString stringWithFormat:@"%@",[[[arrPropertyData objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"no_of_bedroom"] ];
    
    
    if (isarabic)
    {
        cell.lblBed.text=@"قاع";
    }
    else
    {
        cell.lblBed.text=@"Bed";
    }
    
    cell.lblForPrice.text=[NSString stringWithFormat:@"%@",[[[arrPropertyData objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"title"]];
    
    cell.labelForLocationName.text=NSLocalizedString([[[arrPropertyData objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"location"], nil);
    
    NSString *strPropertyType=[[[arrPropertyData objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"property_type"];

    cell.lblForTypeOfFlats.text=LOCALIZATION(strPropertyType) ;
    
    cell.lblForNumOfBedRooms.text=strNumberProperty;
    
    [cell.imageforBuilderName sd_setImageWithURL:[[[arrPropertyData objectAtIndex:indexPath.row] objectForKey:@"Property"] objectForKey:@"image"]];
    
    
    if ((indexPath.row == [arrPropertyData count]-1) && ([arrPropertyData count] != totalRecord))
    {
        [CommonFunctions showActivityIndicatorWithText:@""];
        
        NSString *strAgentID=[NSString stringWithFormat:@"%@",[[arrAgentDetails  objectAtIndex:0] objectForKey:@"id"]];
        [self getAgentDetailwithStrPage:[NSString stringWithFormat:@"%d",currentPage] withAgetntId:strAgentID];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *propertyOwnerID=[NSString stringWithFormat:@"%@",[[[arrPropertyData objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"user_id"]];
    NSString  *propertyID=[NSString stringWithFormat:@"%@",[[[arrPropertyData objectAtIndex:indexPath.row]objectForKey:@"Property"]objectForKey:@"id"]];
    
    [self getPropertyDetails:propertyID andAgentId:propertyOwnerID];
    
    
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(MyListedPropertyCell* )cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CGFloat  height=72;
    return height;
}



- (IBAction)clickToMailHere:(id)sender {
    
    
    EmailAgentVC * email=ICLocalizedViewController([EmailAgentVC class]);//[[EmailAgentVC alloc]init];
    email.strToEmail=@"fromAgent";
    email.arrProperyData=arrAgentDetails ;

    [self.navigationController pushViewController:email animated:YES];

}

- (IBAction)clickToCallHere:(id)sender
{
    NSString *phoneNumber=[NSString stringWithFormat:@"%@",[[arrAgentDetails objectAtIndex:0] valueForKey:@"phone"] ];
    if (nil == phoneNumber || NSNull.null == (id)phoneNumber || [phoneNumber isEqualToString:@"<null>"])
    {
        DLog(@"string is null");
    }
    else
    {
        if (phoneNumber.length>2)
        {
            NSURL *phoneUrl = [NSURL URLWithString:[NSString  stringWithFormat:@"telprompt:%@",phoneNumber]];
            NSString *deviceType = [UIDevice currentDevice].model;
            
            DLog(@"%@",deviceType);
            if ([[UIApplication sharedApplication] canOpenURL:phoneUrl]&&[deviceType isEqualToString:@"iPhone"])
            {
                [[UIApplication sharedApplication] openURL:phoneUrl];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:LOCALIZATION(@"Call facility is not available!!!") delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        }
        
        
    }
}

#pragma mark - WebService API
-(void)getAgentDetailwithStrPage:(NSString*)strPage withAgetntId:(NSString *)strAgentId
{
    if(totalRecord==[arrPropertyData count])
    {
        if (apiCurrentpage==totalPage)
        {
            [CommonFunctions removeActivityIndicator];
            return;
        }
    }
    
    NSMutableDictionary *params;
    params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:strPage,@"pagenumber",strAgentId,@"agentid",language_id,@"language_id",nil];
    
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/propertysearch
    NSString *siteURL = @"agentdetail";
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
            
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            
            if ([self isNotNull:[responseDict objectForKey:@"data"]])
            {
                if ([[[responseDict objectForKey:@"totalRecord"] stringValue] isEqualToString:@"0"]) {
                    lblMsg.text=[responseDict objectForKey:@"replyMsg"];
                    [_AgentDetailTableView reloadData];
                }
                else
                {
                    lblMsg.text=@"";
                    
                    apiCurrentpage=[[responseDict objectForKey:@"firstPage"]intValue];
                    totalPage=[[responseDict objectForKey:@"totalPages"]intValue];
                    totalRecord=[[responseDict objectForKey:@"totalRecord"]intValue];
                    if ([self isNotNull:[responseDict objectForKey:@"data"]])
                    {
                        
                        NSMutableArray *tempArr = [[NSMutableArray alloc]init];
                        // totalPage = [[responseDict objectForKey:@"totalPages"] intValue];
                        
                        NSMutableArray *arr = [[[responseDict objectForKey:@"data"]objectForKey:@"UserPropertyData"]mutableCopy];
                        
                        for (int i = 0; i < [arr count]; i++)
                        {
                            [tempArr addObject:[arr objectAtIndex:i]];
                            
                        }
                        
                        
                        if (currentPage == 1)
                        {
                            [arrPropertyData removeAllObjects];
                            
                        }
                        
                        for (int i = 0; i < [tempArr count]; i++)
                        {
                            [arrPropertyData addObject:[tempArr objectAtIndex:i]];
                        }
                        
                        arrAgentDetails=[[responseDict objectForKey:@"data"] objectForKey:@"UserData"];
                        
                        [self setUserDetails];
                        
                        [tempArr removeAllObjects];
                        
                        tempArr = nil;
                        
                        
                        currentPage++;
                        
                        
                        [self.AgentDetailTableView reloadData];
                    }
                }
            }
            else
            {
                
                if ([[responseDict objectForKey:@"replyMsg"]isEqualToString:@"No result found"]) {
                    //[arrAgentDetails removeAllObjects];
                    [arrPropertyData removeAllObjects];
                    [self.AgentDetailTableView reloadData];
                    
                }
                else
                {
                    if (isarabic)
                    {
                        [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
                    }
                    else
                    {
                        [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
                    }
                    
                }
                
            }
            
            
        }
        
    }withFailure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         if([operation.response statusCode]  == 400 ){
             DLog(@"impo%@",operation.response);
             
             [CommonFunctions removeActivityIndicator];
             if (isarabic)
             {
                 [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
             }
             else
             {
                 [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
             }
             
         }
         else{
             
             [CommonFunctions removeActivityIndicator];
              [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];             
             
         }
         
     }];
    
    
}

#pragma mark- Property detail webservice
-(void)getPropertyDetails:(NSString*)propertyID andAgentId:(NSString*)propertyOwnerID
{
    
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/propertydetail
    NSString *siteURL = @"propertydetail";
    NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"propertyid",propertyOwnerID,@"property_owner_id",language_id,@"language_id",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:nil withServiceName:siteURL withParameters:params withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
        if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];
            
            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            [[NSUserDefaults standardUserDefaults]setObject:[[[responseDict objectForKey:@"data"]objectForKey:@"Property"]objectForKey:@"isFav"]  forKey:UD_FAVORITE];
            [[NSUserDefaults standardUserDefaults]synchronize];
            DetailVC *dtvc=ICLocalizedViewController([DetailVC class]);//[[DetailVC alloc]initWithNibName:@"DetailVC" bundle:nil];
            dtvc.detailDataDict=[responseDict objectForKey:@"data"];
            [self.navigationController pushViewController:dtvc animated:YES];
        }
        else
        {
            
            if (isarabic)
            {
                [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([responseDict objectForKey:@"replyMsg"], nil)];
            }
            else
            {
                [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            }
            
            
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                              [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }
                                              
                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                           
                                          [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];                                              
                                          }
                                          
                                      }];
}

@end
