//
//  EditProfileVC.m
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "EditProfileVC.h"
#import "HomeVC.h"

@interface EditProfileVC ()<UIActionSheetDelegate, UIImagePickerControllerDelegate,UITextFieldDelegate>
{
    NSMutableDictionary *userDataDict;
    IBOutlet UIButton *saveChngesButton;
        NSMutableData *responseData;
    IBOutlet UIView *toolbarVw;
}
@end

@implementation EditProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    if (self.view.frame.size.height<500)
    {
        self.scrollView.scrollEnabled=YES;
    }
    else
    {
        self.scrollView.scrollEnabled=NO;
    }
    self.scrollView.contentSize=_viewForComponents.frame.size;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        //self.edgesForExtendedLayout = UIRectEdgeNone;
    
       _userProfilePic.layer.cornerRadius = _userProfilePic.frame.size.width / 2;
    _userProfilePic.layer.borderColor=[UIColor colorWithRed:(109/255.0f) green:(109/255.0f) blue:(109/255.0f) alpha:1.0f].CGColor;
    _userProfilePic.layer.borderWidth=2.0f;
    _userProfilePic.clipsToBounds = YES;
    
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        isarabic=YES;
    }
    else
    {
        isarabic=NO;
    }

    
    
    [self setUpView];
    self.enterPhoneNum.inputAccessoryView=toolbarVw;
    
}

-(void)setButtonText
{
        NSString *string0;
        NSString *string1;
        NSString *text;
  
            string0 = LOCALIZATION(@"Save") ;
            string1=LOCALIZATION(@"Changes") ;
            
            text = [NSString stringWithFormat:@"%@ %@",string0,string1];
    
        
        
        //whole String attribute
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName:[UIColor colorWithRed:110.0f/255.0f green:110.0f/255.0f blue:110.0f/255.0f alpha:1],
                                  NSFontAttributeName:[UIFont systemFontOfSize:14]
                                  };
        
        NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
        
        NSRange string0Range = [text rangeOfString:string0];
        NSRange string1Range = [text rangeOfString:string1];
        
        [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Bold" size:18.0f]}  range:string0Range];
        [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Light" size:18.0f]}  range:string1Range];
        
        [saveChngesButton setAttributedTitle:attributedText forState:UIControlStateNormal];
        
}

-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBarHidden=NO;
  [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setnavigationBar];
    [self setTitle];
    [self setButtonText];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setUpView
{
    UIView *v1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterAddress.leftViewMode=UITextFieldViewModeAlways;
    _enterAddress.leftView=v1;
    
    UIView *v2=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterEmail.leftViewMode=UITextFieldViewModeAlways;
    _enterEmail.leftView=v2;
    
    UIView *v3=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterName.leftViewMode=UITextFieldViewModeAlways;
    _enterName.leftView=v3;
    
    UIView *v4=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterPhoneNum.leftViewMode=UITextFieldViewModeAlways;
    _enterPhoneNum.leftView=v4;
    
    if ([CommonFunctions reachabiltyCheck]) {
        [CommonFunctions showActivityIndicatorWithText:@""];
            [self getUserProfileData];
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];

    }

}
#pragma mark - set NavigationBar
- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-REGULAR" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
    }

    titleView.text =LOCALIZATION(@"Edit") ;
    self.navigationItem.titleView = titleView;
    
    
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
    [CommonFunctions customizeNavigationController:self.navigationController withImage:@"top_bar_bg" andTintColor:[UIColor redColor]];
    
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    UIButton * rightButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 50, 20)];
    [rightButton addTarget:self action:@selector(goToSave:) forControlEvents:UIControlEventTouchUpInside];
    [rightButton setTitle:@"Save" forState:UIControlStateNormal];
    rightButton.titleLabel.font=[UIFont fontWithName:@"OXYGEN-REGULAR" size:11];
    
    [rightButton setBackgroundImage:[UIImage imageNamed:@"search_filter_applybut"] forState:UIControlStateNormal];
    rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    rightButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
    
    UIEdgeInsets imageInsets;
    
    if([SYSTEM_VERSION floatValue]>=7.0f)
        imageInsets = UIEdgeInsetsMake(0.0f,-6.0f,0.0f, 00.0f);
    else
        imageInsets = UIEdgeInsetsMake(0.0f, -6.0f, 0.0f, 0.0f);
    leftButton.imageEdgeInsets= imageInsets;
    leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    [leftButton addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"back_arrow"] forState:UIControlStateNormal];
    
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];


        [rightButton setTitle:LOCALIZATION(@"Save")  forState:UIControlStateNormal];
    

    
    rightButton.titleLabel.font=[UIFont fontWithName:@"OXYGEN-REGULAR" size:13];
    [rightButton setBackgroundImage:[UIImage imageNamed:@"search_filter_applybut"] forState:UIControlStateNormal];
    
    self.navigationController.navigationBar.translucent=NO;
    
    self.navigationItem.rightBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:rightButton];
    
   
}
-(void)goBack:(id)sender
{
    
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)goToSave:(id)sender
{
    [self.view endEditing:YES];
    [_scrollView setContentOffset:CGPointZero];

    if ([CommonFunctions isValueNotEmpty:_enterName.text]) {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self saveDetailtoServer];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
        }
        
    }
    else
    {
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter name") ];
        [_enterName becomeFirstResponder];
    }
    
    
}
#pragma mark -
#pragma mark - textField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == _enterName) {
        [textField resignFirstResponder];
        [_enterEmail becomeFirstResponder];
    }
    if (textField==_enterEmail) {
        [textField resignFirstResponder];
        [_enterAddress becomeFirstResponder];
    }
    if (textField==_enterAddress) {
        [textField resignFirstResponder];
        [_enterPhoneNum becomeFirstResponder];
    }
    if (textField==_enterPhoneNum) {
        [textField resignFirstResponder];
    }
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if([string length]>=2) {
        [CommonFunctions alertTitle:@"" withMessage:@"Emoji not allowed."];
        return NO;
    }
    
    if (textField ==_enterName||textField==_enterEmail)
    {
        if ([textField.text length]<20)
        {
            return YES;
        }
        else
        {
            
            if (textField.text.length>20)
            {
                for (int i=(int)textField.text.length; i>19;i-- )
                {
                    if (i==19)
                    {
                        break;
                    }
                    else
                    {
                        textField.text = [textField.text substringToIndex:[textField.text  length] - 1];
                    }
                }
            }
            
            return YES;
            
        }
    }
    else if (textField==_enterPhoneNum)
    {
        if ([textField.text length]<11)
        {
            return YES;
        }
        else
        {
            
            if (textField.text.length>11)
            {
                for (int i=(int)textField.text.length; i>10;i-- )
                {
                    if (i==10)
                    {
                        break;
                    }
                    else
                    {
                        textField.text = [textField.text substringToIndex:[textField.text  length] - 1];
                    }
                }
            }
            
            return YES;

        }
    }
     else if (textField==_enterAddress)
    {
        if ([textField.text length]<40)
        {
            return YES;
        }
        else
        {
            if (textField.text.length>40)
            {
                for (int i=(int)textField.text.length; i>39;i-- )
                {
                    if (i==39)
                    {
                        break;
                    }
                    else
                    {
                         textField.text = [textField.text substringToIndex:[textField.text  length] - 1];
                    }
                }
            }
         
            return YES;
        }
    }
   return YES;
}

#pragma mark-textField Validations
-(BOOL)validation
{
    BOOL isValidate = YES;
    NSString *message=nil;
    
       if([_enterName.text length]==0)
    {
        activetxFld=_enterName;
        isValidate=NO;
 
            message=LOCALIZATION(@"Please enter name") ;
    }
    else if([_enterEmail.text length]==0)
    {
        activetxFld=_enterEmail;
        isValidate = NO;
        message=LOCALIZATION(@"Please enter email") ;
    }
    else if([_enterAddress.text length]==0)
    {
        activetxFld=_enterAddress;
        isValidate = NO;
        message=LOCALIZATION(@"Please enter price") ;

    }
    else if ([_enterPhoneNum.text length]==0)
    {
        activetxFld=_enterPhoneNum;
        isValidate = NO;
        message=LOCALIZATION(@"Please enter phone number") ;
    }
    if(message.length>0)
    {
 
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:message delegate:self cancelButtonTitle:LOCALIZATION(@"OK")  otherButtonTitles:nil, nil];
            [alert show];
        
    }
    return isValidate;
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==200)
    {
//        HomeVC *hvc = ICLocalizedViewController([HomeVC class]);//[[HomeVC alloc]initWithNibName:@"HomeVC" bundle:nil];
//        [(UINavigationController *)self.viewDeckController.centerController pushViewController:hvc animated:NO];
//        [(UINavigationController *)self.viewDeckController.centerController setViewControllers :[NSArray arrayWithObject:hvc]];
//        [self.viewDeckController closeLeftViewAnimated:TRUE duration:0.285f completion: ^(IIViewDeckController *controller, BOOL isClosed) {
//        }];
        
    }
    
    else if (activetxFld==_enterName) {
        [_enterName becomeFirstResponder];
    }
    else if(activetxFld==_enterEmail){
        
        [_enterEmail becomeFirstResponder];
    }
    else if(activetxFld==_enterAddress){
        
        [_enterAddress becomeFirstResponder];
    }
    else if(activetxFld==_enterPhoneNum){
        
        [_enterPhoneNum becomeFirstResponder];
    }

    else {
        
        [activetxFld becomeFirstResponder];
    }
    
}

#pragma mark- ActionSheet Delegate methdos
- (IBAction)editHere:(id)sender {
    
    if (isarabic)
    {
        UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:@"اختر صورة" delegate:self cancelButtonTitle:@"الغاء" destructiveButtonTitle:nil otherButtonTitles:@"تصوير",@"اختيار صور", nil];
        [actionSheet showInView:self.view];

    }
    else
    {
        UIActionSheet *actionSheet=[[UIActionSheet alloc]initWithTitle:@"Choose Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Take Photo",@"Choose Photo", nil];
        [actionSheet showInView:self.view];

    }
   
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            

                UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:LOCALIZATION(@"Error!") 
                                                                      message:LOCALIZATION(@"Device has no camera")
                                                                     delegate:nil
                                                            cancelButtonTitle:LOCALIZATION(@"OK")
                                                            otherButtonTitles: nil];
                [myAlertView show];
  
        }
        else
        {
            UIImagePickerController *imgPicker=[[UIImagePickerController alloc]init];
            imgPicker.delegate = self;
            imgPicker.allowsEditing = YES;
            imgPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:imgPicker animated:YES completion:NULL];
        }

    }
    else if (buttonIndex==1)
    {
        UIImagePickerController *imgPicker=[[UIImagePickerController alloc]init];
        imgPicker.delegate = self;
        imgPicker.allowsEditing = YES;
        imgPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:imgPicker animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    _userProfilePic.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}



- (IBAction)saveChanges:(id)sender {
    [self.view endEditing:YES];
    [_scrollView setContentOffset:CGPointZero];

    if ([CommonFunctions isValueNotEmpty:_enterName.text]) {
        if ([CommonFunctions reachabiltyCheck]) {
            [CommonFunctions showActivityIndicatorWithText:@""];
            [self saveDetailtoServer];
        }
        else
        {
             [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
        }
        
    }
    else
    {
 
        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter name") ];
        [_enterName becomeFirstResponder];
    }
}

#pragma mark- WebService API
-(void)getUserProfileData
{
    
    //http://mymeetingdesk.com/mobile/baiti/mobile/userProfile
    NSString *siteURL = @"userProfile";
    //NSMutableDictionary *params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:propertyID, @"propertyid",_addNotesTextView.text,@"description",@"1",@"noteid",nil];
    NSMutableDictionary *header=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSUserDefaults standardUserDefaults] objectForKey:UD_TOKEN], @"token",language_id,@"language_id",nil];
    
    ConnectionManager *connectionManager = [ConnectionManager sharedInstance];
    
    [connectionManager startRequestWithHttpMethod:kHttpMethodTypePost withHttpHeaders:header withServiceName:siteURL withParameters:nil withSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        
        NSDictionary  *responseDict = [NSJSONSerialization JSONObjectWithData: responseObject options: NSJSONReadingMutableContainers error: nil];
        DLog(@"responce dict %@",responseDict);
        
        [CommonFunctions removeActivityIndicator];
if(responseDict==Nil)        {
      [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"System Error! Please Contact to admin")];

            
        }
        else if([[responseDict objectForKey:@"replyCode"] isEqualToString:@"success"])
        {
            userDataDict =[responseDict objectForKey:@"User"];
            if ([self isNotNull:[userDataDict objectForKey:@"phone"]])
            {
            _enterPhoneNum.text=[userDataDict objectForKey:@"phone"];
            }
            else
            {
                _enterPhoneNum.text=@"";
            }
     
        // get emoji from string
            NSData *data = [[userDataDict objectForKey:@"username"] dataUsingEncoding:NSUTF8StringEncoding];
            NSString *valueEmoj = [[NSString alloc] initWithData:data encoding:NSNonLossyASCIIStringEncoding];
            
            _enterName.text=valueEmoj;
            _enterEmail.text=[userDataDict objectForKey:@"email"];
            _enterAddress.text=[userDataDict objectForKey:@"address"];
            [_userProfilePic setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[userDataDict objectForKey:@"image"]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[responseDict objectForKey:@"replyMsg"]];
            
        }
        
        
    }
                                      withFailure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                          if([operation.response statusCode]  == 400 ){
                                              DLog(@"impo%@",operation.response);
                                              
                                        [CommonFunctions removeActivityIndicator];
                                              if (isarabic)
                                              {
                                                  [CommonFunctions alertArabicTitle:@"" withMessage:NSLocalizedString([operation.responseObject objectForKey:@"response"], nil)];
                                              }
                                              else
                                              {
                                                  [CommonFunctions alertTitle:@"" withMessage:[operation.responseObject objectForKey:@"response"]];
                                              }

                                          }
                                          else{
                                              
                                              [CommonFunctions removeActivityIndicator];
                                                   [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")]; 
                                          }
                                          
                                      }];
}




-(void)saveDetailtoServer;
{
    
    if (responseData!=nil) {
        
        responseData = nil;
    }
    
    responseData = [[NSMutableData alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // //DLog(@"TOKEN IS %@",[prefs valueForKey:@"userToken"]);
    
    
    NSString *useLatitude=[NSString stringWithFormat:@"%f",CurrentLatitude];
    NSString *userLongitude=[NSString stringWithFormat:@"%f",CurrentLongitude];
    
    NSString *strName=[CommonFunctions trimSpaceInString:_enterName.text];
    
    
    //set emogi in string
    NSData *data = [strName dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *valueUnicode = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    
    [request setValue:[[NSUserDefaults standardUserDefaults] valueForKey:UD_TOKEN] forHTTPHeaderField:@"token"];
    
    NSMutableDictionary *_params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:valueUnicode,@"username",[CommonFunctions trimSpaceInString:_enterAddress.text],@"address",
                                  [CommonFunctions trimSpaceInString:_enterEmail.text],@"email",[CommonFunctions trimSpaceInString:_enterPhoneNum.text],@"phone",useLatitude,@"mlat",userLongitude,@"mlong",userDeviceToken,@"device_id",language_id,@"language_id",nil];
    
    //[_params setObject:@"image.png" forKey:@"image"];
    
    DLog(@"params %@",_params);
    
    //    [_params setObject:[prefs valueForKey:@"userToken"] forKey:@"header"];
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'
    //    NSString *FileParamConstant = @"image";
    
    
    NSURL *requestURL = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"%@updateUserProfile_ios",SiteAPIURL]];
    
    // //DLog(@"request url %@",requestURL);
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data
        UIImage *selectedImage = _userProfilePic.image;
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@\"\r\n\r\n",[NSString stringWithFormat:@"image34.jpeg"]] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:UIImageJPEGRepresentation(selectedImage, 1.0)];
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
 
    
    [request setHTTPBody:body];
    
    // setting the body of the post to the reqeust
    
    
    // set the content-length
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self startImmediately:YES];
    [conn start];
    
}

-(void)connection:(NSURLConnection *) connection didReceiveData: (NSData*)data
{
    [responseData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{

        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Connection Failed")  message:LOCALIZATION(@"Please check your network connection or try again later")  delegate:self cancelButtonTitle:LOCALIZATION(@"OK")  otherButtonTitles:nil];
        [alert show];
       [CommonFunctions removeActivityIndicator];
    
}
-(void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSString *result=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    DLog(@"resulted dict is %@",result);
    NSMutableDictionary *resultedDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:resultedDict];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:UD_USERINFO];
    
    
    [[NSUserDefaults standardUserDefaults]setObject:[resultedDict objectForKey:@"my_fav_property"] forKey:UD_FAV_PROPERTY];
    [[NSUserDefaults standardUserDefaults]setObject:[resultedDict objectForKey:@"my_listed_property"] forKey:UD_LISTED_PROPERTY];
    
    [[NSUserDefaults standardUserDefaults]synchronize];
//    [[NSUserDefaults standardUserDefaults]setObject:[resultedDict objectForKey:@"my_fav_property"] forKey:UD_FAV_PROPERTY];
//    [[NSUserDefaults standardUserDefaults]setObject:[resultedDict objectForKey:@"my_listed_property"] forKey:UD_LISTED_PROPERTY];
//    [[NSUserDefaults standardUserDefaults]synchronize];

    if (isarabic)
    {
        NSString *strmessage=[NSString stringWithFormat:@"%@",[resultedDict objectForKey:@"replyMsg"]];
        [CommonFunctions alertTitleArabic:@"" withMessage:NSLocalizedString(strmessage, nil) withDelegate:self withTag:200];
    }
    else
    {
           [CommonFunctions alertTitle:@"" withMessage:[NSString stringWithFormat:@"%@",[resultedDict objectForKey:@"replyMsg"]]withDelegate:self withTag:200];
    }
 
    [CommonFunctions removeActivityIndicator];
    
}
- (IBAction)doneButtonClicked:(id)sender {
    [self.view endEditing:YES];
    [_scrollView setContentOffset:CGPointZero];
}
- (IBAction)cancelButtonClicked:(id)sender {    [self.view endEditing:YES];
_enterPhoneNum.text=@"";
    [_scrollView setContentOffset:CGPointZero];}



@end
