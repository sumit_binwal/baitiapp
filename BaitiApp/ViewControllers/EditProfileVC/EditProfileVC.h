//
//  EditProfileVC.h
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditProfileVC : UIViewController<UIAlertViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UITextField * activetxFld;
    
    BOOL isarabic;
}



@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *viewForComponents;

@property (strong, nonatomic) IBOutlet UIImageView *backgroundImage;
@property (strong, nonatomic) IBOutlet UIImageView *userProfilePic;
@property (strong, nonatomic) IBOutlet UITextField *enterName;
@property (strong, nonatomic) IBOutlet UITextField *enterEmail;
@property (strong, nonatomic) IBOutlet UITextField *enterAddress;
@property (strong, nonatomic) IBOutlet UITextField *enterPhoneNum;

- (IBAction)editHere:(id)sender;
- (IBAction)saveChanges:(id)sender;




@end
