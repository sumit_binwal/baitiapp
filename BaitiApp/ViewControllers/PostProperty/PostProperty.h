//
//  PostProperty.h
//  BaitiApp
//
//  Created by Shweta Rao on 30/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostPropertyCollViewCell.h"
#import "PostPropertyStep2VC.h"

@interface PostProperty : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIAlertViewDelegate>
{
    IBOutlet UIButton *cameraBtn;
    IBOutlet UIButton *galleryBtn;
    UITextField * activetxFld;
    BOOL isarabic;

}
@property (strong, nonatomic) IBOutlet UITextField *enterLocation;
@property (strong, nonatomic) IBOutlet TextFieldAlignment *enterPrice;
@property (strong, nonatomic) IBOutlet UITextField *txtPropertyType;

@property (strong, nonatomic) IBOutlet UICollectionView *collView;
@property (strong, nonatomic) IBOutlet UIView *viewForComponents;


@property (strong, nonatomic) IBOutlet UIButton *saleBtn;
@property (strong, nonatomic) IBOutlet UIButton *rentBtn;


@property (strong, nonatomic)NSString *propertyMode;

@property (strong, nonatomic)NSArray *propertyTypeArr;
@property (strong, nonatomic)NSArray *LocationArr;
@property (strong, nonatomic)NSMutableArray *imageArr;
@property (strong, nonatomic)NSMutableDictionary *detailDataDict;


@property (strong, nonatomic) IBOutlet NSString *userlet;
@property (strong, nonatomic) IBOutlet NSString *userlong;
@property (strong, nonatomic) IBOutlet NSString *locationID;

- (IBAction)forSale:(id)sender;
- (IBAction)forRent:(id)sender;
- (IBAction)cameraBtnClicked:(id)sender;
- (IBAction)galleryBtnClicked:(id)sender;
- (IBAction)goToPostPropertyStep2:(id)sender;

@property (strong,nonatomic) NSMutableArray *imagesArray;
@property UIImagePickerController * picker;
@property UIImageView *imageView;

@end
