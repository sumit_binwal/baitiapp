//
//  SettingViewVC.h
//  BaitiApp
//
//  Created by Sumit Sharma on 12/06/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewVC : UIViewController
@property(nonatomic,strong)NSString *strWbUrl;
@property(nonatomic,strong)NSString *strNavTitle;
@end
