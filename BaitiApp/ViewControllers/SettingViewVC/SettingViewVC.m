//
//  SettingViewVC.m
//  BaitiApp
//
//  Created by Sumit Sharma on 12/06/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "SettingViewVC.h"

@interface SettingViewVC ()<UIWebViewDelegate>
{
    
    IBOutlet UIWebView *wbViewSetting;
    
}
@end

@implementation SettingViewVC
@synthesize strWbUrl,strNavTitle;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

-(void)viewWillAppear:(BOOL)animated
{

    [super viewWillAppear:YES];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    [self.navigationItem setTitle:strNavTitle];
    if (strWbUrl.length>0) {
        NSURL *url=[NSURL URLWithString:strWbUrl];
        NSURLRequest *request=[NSURLRequest requestWithURL:url];

        [CommonFunctions showActivityIndicatorWithText:LOCALIZATION(@"Please Wait")];
        
        [wbViewSetting loadRequest:request];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    DLog(@"Finished load");
    [CommonFunctions removeActivityIndicator];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
