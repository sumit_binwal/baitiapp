//
//  PostPropertyStep2VC.h
//  BaitiApp
//
//  Created by Shweta Rao on 31/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PostPropertyStep2VC : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>
{
    UITextField* activeTextFld;
    BOOL isarabic;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *viewForComponents;
@property (strong, nonatomic) IBOutlet UITextView *enterDescription;
@property (strong, nonatomic) IBOutlet UITextField *enterSize;
@property (strong, nonatomic) IBOutlet UITextField *enterName;
@property (strong, nonatomic) IBOutlet UITextField *enterPhoneNum;
@property (strong, nonatomic) IBOutlet UITextField *enterEmailAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnOne;
@property (strong, nonatomic) IBOutlet UIButton *btnTwo;
@property (strong, nonatomic) IBOutlet UIButton *btnThree;
@property (strong, nonatomic) IBOutlet UIButton *btnFour;
@property (strong, nonatomic) IBOutlet UIButton *btnFive;
@property (strong, nonatomic) IBOutlet UIButton *btnSix;
@property (strong,nonatomic)  NSMutableArray *postPropertyDataArr;
@property (strong,nonatomic)  NSMutableDictionary *propertyDataDict;

- (IBAction)btnOneSelected:(id)sender;


- (IBAction)postPropertyAction:(id)sender;


@end
