//
//  PostPropertyStep2VC.m
//  BaitiApp
//
//  Created by Shweta Rao on 31/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "PostPropertyStep2VC.h"
#import "PostProperty.h"


@interface PostPropertyStep2VC ()<UIGestureRecognizerDelegate,UITextViewDelegate>
{
    IBOutlet UITextView *txtDiscription;
    NSString *numberofBedrooms;
    IBOutlet UIView *toolbarViw;
    
    NSMutableData *responseData;
    
    IBOutlet UIButton *btnSubmit;
}
@end

@implementation PostPropertyStep2VC
@synthesize postPropertyDataArr,propertyDataDict;
- (void)viewDidLoad {
    [super viewDidLoad];
    DLog(@"----=7=-==-=Data : %@",propertyDataDict);
    self.scrollView.contentSize=_viewForComponents.frame.size;
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        
        self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIView *view1=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterSize.leftViewMode=UITextFieldViewModeAlways;
    _enterSize.leftView=view1;

    UIView *view2=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterName.leftViewMode=UITextFieldViewModeAlways;
    _enterName.leftView=view2;
    
    UIView *view3=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterEmailAddress.leftViewMode=UITextFieldViewModeAlways;
    _enterEmailAddress.leftView=view3;
    
    UIView *view4=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 10.0f, 0)];
    _enterPhoneNum.leftViewMode=UITextFieldViewModeAlways;
    _enterPhoneNum.leftView=view4;
 
    
    _enterPhoneNum.inputAccessoryView=toolbarViw;
    _enterSize.inputAccessoryView=toolbarViw;
    
    
    UITapGestureRecognizer *tapGesture=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureClicked)];
    tapGesture.delegate=self;
    tapGesture.numberOfTapsRequired=1;
    [self.view addGestureRecognizer:tapGesture];
    
    
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [_scrollView setScrollEnabled:YES];
        [_scrollView setContentSize:CGSizeMake(320.0f, 500.0f)];
    }
    else
    {
        [_scrollView setScrollEnabled:NO];
    }
    
    if (propertyDataDict.count>0) {
        txtDiscription.text=NSLocalizedString([[propertyDataDict objectForKey:@"Property"] objectForKey:@"description"], nil);
        _enterSize.text=[[propertyDataDict objectForKey:@"Property"] objectForKey:@"size"];

        _enterName.text=NSLocalizedString([[propertyDataDict objectForKey:@"Property"] objectForKey:@"firstname"],nil);

        _enterPhoneNum.text=[[propertyDataDict objectForKey:@"Property"] objectForKey:@"phone"];

        _enterEmailAddress.text=[[propertyDataDict objectForKey:@"Property"] objectForKey:@"email"];
        
        int badrom=[[[propertyDataDict objectForKey:@"Property"] objectForKey:@"no_of_bedroom"] intValue];
        switch (badrom) {
            case 1:
                [_btnOne setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
                [_btnOne setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                numberofBedrooms=@"1";
                break;
            case 2:
                [_btnTwo setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
                [_btnTwo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                numberofBedrooms=@"2";
                break;

            case 3:
                [_btnThree setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
                [_btnThree setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                numberofBedrooms=@"3";
                break;

            case 4:
                [_btnFour setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
                [_btnFour setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                numberofBedrooms=@"4";
                break;

            case 5:
                [_btnFive setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
                [_btnFive setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                numberofBedrooms=@"5";
                break;
            case 6:
                [_btnSix setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
                [_btnSix setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                numberofBedrooms=@"6";
                break;

                
            default:
                break;
        }

    }
}


-(void)setButtonText
{
    NSString *string0;
    NSString *string1;
    NSString *text;

        string0 = LOCALIZATION(@"Post") ;
        string1=LOCALIZATION(@"Property") ;
        
        text = [NSString stringWithFormat:@"%@ %@",string0,string1];
        
    
    
    
    //whole String attribute
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[UIColor colorWithRed:110.0f/255.0f green:110.0f/255.0f blue:110.0f/255.0f alpha:1],
                              NSFontAttributeName:[UIFont systemFontOfSize:14]
                              };
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
    
    NSRange string0Range = [text rangeOfString:string0];
    NSRange string1Range = [text rangeOfString:string1];
    
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Bold" size:18.0f]}  range:string0Range];
    [attributedText setAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],NSFontAttributeName:[UIFont fontWithName:@"Oxygen-Light" size:18.0f]}  range:string1Range];
    
    [btnSubmit setAttributedTitle:attributedText forState:UIControlStateNormal];
    
}



-(void)tapGestureClicked
{
    [self.view endEditing:YES];
    [_scrollView setContentOffset:CGPointZero];
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [_scrollView setScrollEnabled:YES];
    }
    else
    {
        [_scrollView setScrollEnabled:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    
    self.navigationController.navigationBarHidden=NO;
    [[UIApplication sharedApplication]setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self setnavigationBar];
    [self setTitle];
    [self setButtonText];
    
}

#pragma mark - setNavigationbar

- (void)setTitle
{
    UILabel *titleView = (UILabel *)self.navigationItem.titleView;
    if (!titleView) {
        titleView = [[UILabel alloc] initWithFrame:CGRectZero];
        titleView.backgroundColor = [UIColor clearColor];
        titleView.font = [UIFont fontWithName:@"OXYGEN-LIGHT" size:18];
        titleView.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.0];
        
        titleView.textColor = [UIColor whiteColor]; // Change to desired color
        
        self.navigationItem.titleView = titleView;
        
    }

        titleView.text =LOCALIZATION(@"Post Property") ;
        isarabic=NO;
    
    [titleView sizeToFit];
}
-(void)setnavigationBar
{
    UINavigationBar *navBar=self.navigationController.navigationBar;
    //self.navigationController.navigationBar.barTintColor=[UIColor colorWithRed:227.0f/255.0f green:0.0f/255.0f blue:134.0f/255.0f alpha:1];
    navBar.tintColor=[UIColor whiteColor];
    
    UIImage *img=[UIImage imageNamed:@"top_bar_bg"];
    
    [navBar setBackgroundImage:img forBarMetrics:UIBarMetricsDefault];
    [self.navigationController setNavigationBarHidden:FALSE];
    UIButton * leftButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [leftButton addTarget:self.viewDeckController action:@selector(toggleLeftView) forControlEvents:UIControlEventTouchUpInside];
    [leftButton setImage:[UIImage imageNamed:@"list_icon"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem=[[UIBarButtonItem alloc]initWithCustomView:leftButton];
    
    
}

#pragma mark -textField Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
 
    if(alertView.tag==100)
    {
        [_enterName becomeFirstResponder];
    }
    else if (alertView.tag==101)
    {
        [_enterPhoneNum becomeFirstResponder];
    }
    else if (alertView.tag==102)
    {
        [_enterEmailAddress becomeFirstResponder];
    }
    else if (alertView.tag==200)
    {
        IIViewDeckController *controller=[APPDELEGATE generateControllerStack];
        APPDELEGATE.window.rootViewController=controller;
    }
}
- (IBAction)btnOneSelected:(id)sender
{
    
    
    UIButton *btn=(UIButton *)sender;
    int btntag=(int)btn.tag;
    
    [_btnOne setBackgroundImage:[UIImage imageNamed:@"bedroom_number"] forState:UIControlStateNormal];
    [_btnOne setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnTwo setBackgroundImage:[UIImage imageNamed:@"bedroom_number"] forState:UIControlStateNormal];
    [_btnTwo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnThree setBackgroundImage:[UIImage imageNamed:@"bedroom_number"] forState:UIControlStateNormal];
    [_btnThree setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnFour setBackgroundImage:[UIImage imageNamed:@"bedroom_number"] forState:UIControlStateNormal];
    [_btnFour setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnFive setBackgroundImage:[UIImage imageNamed:@"bedroom_number"] forState:UIControlStateNormal];
    [_btnFive setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnSix setBackgroundImage:[UIImage imageNamed:@"bedroom_number"] forState:UIControlStateNormal];
    [_btnSix setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

    
    switch (btntag) {
            
        case 1:
            [btn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
            numberofBedrooms=@"1";
            break;
        case 2:
            [btn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

         numberofBedrooms=@"2";
            break;
        case 3:
            [btn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         numberofBedrooms=@"3";
            break;
        case 4:
            [btn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
           numberofBedrooms=@"4";
            break;
        case 5:
            [btn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
         numberofBedrooms=@"5";
            break;
            
            case 6:
            
            [btn setBackgroundImage:[UIImage imageNamed:@"bedroom_number_select"] forState:UIControlStateNormal];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            numberofBedrooms=@"6";
            break;
    }
}


-(void)scrollViewToCenterOfScreen:(UITextField *)txtField
{
    float difference;
    
    if (_scrollView.contentSize.height == 600)
        difference = 50.0f;
    else
        difference = 60.0f;
    
    CGFloat viewCenterY = txtField.center.y;
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    CGFloat avaliableHeight = applicationFrame.size.height - 100.0f;
    CGFloat y = viewCenterY - avaliableHeight / 4.0f;
    
    if (y < 0)
        y = 0;
    
    [_scrollView setContentOffset:CGPointMake(0, y) animated:YES];
    
    
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [_scrollView setContentSize:CGSizeMake(320.0f, 800.0f)];
    }
    else
    {
    [_scrollView setContentSize:CGSizeMake(320.0f, 750.0f)];
    }
}
#pragma mark- TextField Delegate Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == _enterSize) {
        [textField resignFirstResponder];
        [_enterName becomeFirstResponder];
    }
    if (textField==_enterName) {
        [textField resignFirstResponder];
        [_enterPhoneNum becomeFirstResponder];
    }
    if (textField==_enterPhoneNum) {
        [textField resignFirstResponder];
        [_enterEmailAddress becomeFirstResponder];
    }
    if (textField==_enterEmailAddress) {
        [textField resignFirstResponder];
        [_scrollView setContentOffset:CGPointZero];
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.scrollView setScrollEnabled:YES];
    [self scrollViewToCenterOfScreen:textField];
    activeTextFld=textField;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [_scrollView setScrollEnabled:YES];
    }
    else
    {
        [_scrollView setScrollEnabled:NO];
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField==_enterSize)
    {
        if(range.location>8)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else if (textField==_enterName)
    {
        if(range.location>25)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else if (textField==_enterPhoneNum)
    {
        if(range.location>12)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else if (textField==_enterEmailAddress)
    {
        if(range.location>40)
        {
            return NO;
        }
        else
        {
            return YES;
        }
    }
    else
    {
        return YES;
    }
}
#pragma mark- UITextView Delegate Methods
-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    DLog(@"%@",text);
    if([text isEqualToString:@"\n"])
    {
        [_enterSize becomeFirstResponder];
    }
    return YES;
}


#pragma mark- IBAction butons
- (IBAction)cancelButtonClicked:(id)sender {
    [self.view endEditing:YES];
    [_scrollView setContentOffset:CGPointZero];
    if([UIScreen mainScreen].bounds.size.height<568)
    {
        [_scrollView setScrollEnabled:YES];
    }
    else
    {
        [_scrollView setScrollEnabled:NO];
    }

}


- (IBAction)doneButtonClicked:(id)sender {
    if(activeTextFld==_enterSize)
    {
        [_scrollView setContentOffset:CGPointZero];
        [_enterSize resignFirstResponder];
    }
    else
    {
        [_enterEmailAddress becomeFirstResponder];
    }
    
}

- (IBAction)postPropertyAction:(id)sender {
    [self.view endEditing:YES];
    [self.scrollView setContentOffset:CGPointZero];
    
    if (![CommonFunctions isValueNotEmpty:numberofBedrooms]) {

            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please select Number of BedRooms") withDelegate:self withTag:0];
 
        return;
        
    }
    
    
    if([CommonFunctions isValueNotEmpty:_enterName.text])
    {
        if ([CommonFunctions isValueNotEmpty:_enterPhoneNum.text]) {
            
            if([CommonFunctions isValueNotEmpty:_enterEmailAddress.text])
            {
                if([CommonFunctions IsValidEmail:_enterEmailAddress.text])
                {
                    if([CommonFunctions reachabiltyCheck])
                    {
                         [CommonFunctions showActivityIndicatorWithText:@""];
                        [self addPostOnServer];
                    }
                    else
                    {
                        [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please check your network connection or try again later")];
                    }
                }
                else
                {

                    [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter valid email")  withDelegate:self withTag:102];
 
                }
            }
            else
            {

                   [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter email")  withDelegate:self withTag:1055];
                
                [_enterEmailAddress becomeFirstResponder];

            }
        }
        else
        {

                [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter phone number")  withDelegate:self withTag:101];
            
        }
    }
    else
    {

            [CommonFunctions alertTitle:@"" withMessage:LOCALIZATION(@"Please enter name")  withDelegate:self withTag:100];
        
    }
    
}

#pragma mark- WebServer API

-(void)addPostOnServer
{
    
    if (responseData!=nil) {
        
        responseData = nil;
    }
    
    responseData = [[NSMutableData alloc] init];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // //DLog(@"TOKEN IS %@",[prefs valueForKey:@"userToken"]);
    
    
    [request setValue:[[NSUserDefaults standardUserDefaults] valueForKey:UD_TOKEN] forHTTPHeaderField:@"token"];


    NSMutableDictionary *_params;
    if (propertyDataDict.count>0) {
        _params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[postPropertyDataArr objectAtIndex:0]objectForKey:@"title"],@"title",[[postPropertyDataArr objectAtIndex:0]objectForKey:@"property_mode"],@"property_mode",[[postPropertyDataArr objectAtIndex:0]objectForKey:@"property_type"],@"property_type",[[postPropertyDataArr objectAtIndex:0]objectForKey:@"location"],@"location_id",[[postPropertyDataArr objectAtIndex:0]objectForKey:@"price"],@"price",txtDiscription.text,@"description",_enterSize.text,@"size",numberofBedrooms,@"no_of_bedroom",_enterPhoneNum.text,@"phone_number",_enterEmailAddress.text,@"email",_enterName.text,@"name",[[propertyDataDict objectForKey:@"Property"] objectForKey:@"id"],@"property_id",language_id,@"language_id",nil];

    }
    else
    {
        if (!txtDiscription.text.length>0) {
            txtDiscription.text=@"";
        }
        if (!_enterSize.text.length>0) {
            _enterSize.text=@"";
        }
        if (!numberofBedrooms.length>0) {
            numberofBedrooms=@"";
        }
        
        _params=[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[postPropertyDataArr objectAtIndex:0]objectForKey:@"title"],@"title",[[postPropertyDataArr objectAtIndex:0]objectForKey:@"property_mode"],@"property_mode",[[postPropertyDataArr objectAtIndex:0]objectForKey:@"property_type"],@"property_type",[[postPropertyDataArr objectAtIndex:0]objectForKey:@"location"],@"location_id",[[postPropertyDataArr objectAtIndex:0]objectForKey:@"price"],@"price",txtDiscription.text,@"description",_enterSize.text,@"size",numberofBedrooms,@"no_of_bedroom",_enterPhoneNum.text,@"phone_number",_enterEmailAddress.text,@"email",_enterName.text,@"name",language_id,@"language_id",nil];
        
    }
    
    //[_params setObject:@"image.png" forKey:@"image"];
    
    DLog(@"params %@",_params);
    
    //    [_params setObject:[prefs valueForKey:@"userToken"] forKey:@"header"];
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'
    //    NSString *FileParamConstant = @"image";
    
    
    NSURL *requestURL = [[NSURL alloc]initWithString:[NSString stringWithFormat:@"%@addlisting_ios",SiteAPIURL]];
    
    // //DLog(@"request url %@",requestURL);
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data
    DLog(@"%@",[[postPropertyDataArr objectAtIndex:0]objectForKey:@"picture"]);
    for (int i=0; i<[[[postPropertyDataArr objectAtIndex:0]objectForKey:@"picture"]count]; i++) {
        UIImage *selectedImage = [[[postPropertyDataArr objectAtIndex:0] objectForKey:@"picture"]objectAtIndex:i];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image[]\"; filename=\"%@\"\r\n\r\n",[NSString stringWithFormat:@"image%u.jpeg",i]] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:UIImageJPEGRepresentation(selectedImage, 1.0)];
        [body appendData:[@"Content-Type:image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        DLog(@"%d",i);
    }
    
    [request setHTTPBody:body];
    
    // setting the body of the post to the reqeust
    
    
    // set the content-length
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    NSURLConnection *conn = [[NSURLConnection alloc]initWithRequest:request delegate:self startImmediately:YES];
    [conn start];
    
}
-(void)connection:(NSURLConnection *) connection didReceiveData: (NSData*)data
{
    [responseData appendData:data];
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{

        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:LOCALIZATION(@"Connection Failed")  message:LOCALIZATION(@"Please check your network connection or try again later")  delegate:self cancelButtonTitle:LOCALIZATION(@"OK")  otherButtonTitles:nil];
        [alert show];
    [CommonFunctions removeActivityIndicator];
}
-(void) connectionDidFinishLoading:(NSURLConnection *)connection
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSString *result=[[NSString alloc]initWithData:responseData encoding:NSUTF8StringEncoding];
    DLog(@"resulted dict is %@",result);
    
    NSMutableDictionary *resultedDict = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:nil];
    
    if ([[resultedDict objectForKey:@"replyCode"]isEqualToString:@"error"]) {
        [CommonFunctions alertTitle:@"" withMessage:[NSString stringWithFormat:@"%@",[resultedDict objectForKey:@"replyMsg"]]];
        
    }
    else
    {
        [[NSUserDefaults standardUserDefaults]setObject:[resultedDict objectForKey:@"my_fav_property"] forKey:UD_FAV_PROPERTY];
        [[NSUserDefaults standardUserDefaults]setObject:[resultedDict objectForKey:@"my_listed_property"] forKey:UD_LISTED_PROPERTY];
        [[NSUserDefaults standardUserDefaults]synchronize];
        if (isarabic)
        {
            [CommonFunctions alertTitleArabic:@"" withMessage:NSLocalizedString([resultedDict objectForKey:@"replyMsg"], nil) withDelegate:self withTag:200];
        }
        else
        {
            [CommonFunctions alertTitle:@"" withMessage:[resultedDict objectForKey:@"replyMsg"] withDelegate:self withTag:200];
        }
        [CommonFunctions removeActivityIndicator];
    }
    
    [CommonFunctions removeActivityIndicator];
    
    
   }


@end
