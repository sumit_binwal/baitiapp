//
//  DBManager.h
//  FMDB Database_Image Insert
//
//  Created by Sumit Sharma on 12/10/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface DBManager : NSObject
{
        NSMutableArray *categoryMA;
    
}
-(NSMutableArray *)fetchLocation:(NSString*)str;
+(DBManager *)getSharedInstance;
-(void)checkAndCreateDatabase;
-(NSMutableArray *)fetchCategoryFromDB:(int)catid;
-(BOOL)truncateCategoryTable:(int)cate;
-(NSMutableArray *)fetchIsSelectCategory;
-(BOOL)insertCategoryWithIscheck:(NSMutableArray *)checkMA;
-(BOOL)insertCategory:(NSMutableArray *)uncheckMA;
-(BOOL)insertSubCategory:(NSMutableArray *)uncheckMA;
-(NSMutableArray *)fetchSubCategoryFromDB:(int)catid;

-(BOOL)updateCategoryWithIscheck:(NSMutableArray *)checkMA;
-(BOOL)updateSubCategoryWithIscheck:(NSMutableArray *)checkMA;

-(NSMutableArray *)fetchListForSelectCategory:(NSInteger)type;
-(NSMutableArray *)fetchListForSelectedSubCategoryFromDB:(int)catid;
-(BOOL)checkSubCategoryInDB:(NSMutableDictionary *)subCatDict;
-(BOOL)updateCategoryWithIscheckWhenSbCateSelected:(int)catId;
-(BOOL)updateCategoryWithIscheckWhenNoAnySbCateSelected:(int)catId;
-(BOOL)insertBookmark:(NSDictionary *)bookmarkDict;
-(BOOL)truncateLocationTableData;
-(BOOL)checkBookmark:(NSString *)nwsid;
-(NSMutableArray *)fetchBookmarkDetails;
-(BOOL)checkSubCategoryValueInDB:(int)catID;
@end
