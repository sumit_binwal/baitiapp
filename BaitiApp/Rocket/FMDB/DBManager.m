//
//  DBManager.m
//  FMDB Database_Image Insert
//
//  Created by Sumit Sharma on 12/10/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt Ltd. All rights reserved.
//

#import "DBManager.h"
#import "FMDatabase.h"
@interface DBManager ()

@end

@implementation DBManager

static DBManager *sharedInstance=nil;

+(DBManager *)getSharedInstance
{
    if(sharedInstance==nil)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            sharedInstance=[[DBManager alloc]init];
        });
    }
    return sharedInstance;
}

#pragma mark -Database Methods
/**
 *  Check And Create Databse
 */
-(void)checkAndCreateDatabase
{
        NSString *documentDirectory=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)objectAtIndex:0];
        databasePath=[documentDirectory stringByAppendingFormat:@"/%@",databaseName];
        if([[NSFileManager defaultManager]fileExistsAtPath:databasePath])
        {
            NSLog(@"databse Path : %@",databasePath);

            return;
        }
        NSString *databasePathFromApp=[[[NSBundle mainBundle]resourcePath]stringByAppendingPathComponent:databaseName];
        NSError *error;
        [[NSFileManager defaultManager]copyItemAtPath:databasePathFromApp toPath:databasePath error:&error];
        if(error) {
            
            NSLog(@"The Error In Database Creation : ------- %@",error);
            
        }
        else
        {
            
            NSLog(@"Database SucessFully Copied");

        }
}
/**
 *  Fetch Category From Database
 *
 *  @return filterName, FilterID
 */
-(NSMutableArray *)fetchCategoryFromDB:(int)catid
{
    categoryMA=[[NSMutableArray alloc]init];
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qryString=[NSString stringWithFormat:@"select * from category where filter_type=%d",catid];
    FMResultSet *FmResultSet=[database executeQuery:qryString];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            NSString *filterName = [FmResultSet stringForColumn:@"filter_name"];
            NSString *filterid = [FmResultSet stringForColumn:@"filter_id"];
            NSInteger isSelected = [FmResultSet intForColumn:@"ischeck"];
            
            [categoryMA addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:filterName,@"catetitle",filterid,@"cateid", [NSNumber numberWithInt:isSelected], @"isSelected", nil]];
        }
    }
    
    return categoryMA;
}

-(NSMutableArray *)fetchSubCategoryFromDB:(int)catid
{
    categoryMA=[[NSMutableArray alloc]init];
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qryString=[NSString stringWithFormat:@"select * from subcate where filter_id=%d",catid];
    FMResultSet *FmResultSet=[database executeQuery:qryString];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            NSString *filterName = [FmResultSet stringForColumn:@"subfilter_name"];
            NSString *filterid = [FmResultSet stringForColumn:@"subfilter_id"];
            NSInteger isSelected = [FmResultSet intForColumn:@"ischeck"];
            
            [categoryMA addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:filterName,@"subcatetitle",filterid,@"subcateid",[NSNumber numberWithInt:isSelected], @"isSelected",[NSNumber numberWithInt:catid], @"cateId", nil]];
        }
    }
    return categoryMA;
}

-(NSMutableArray *)fetchSubCategoryFromDB
{
    categoryMA=[[NSMutableArray alloc]init];
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qryString=@"select * from subcate";
    FMResultSet *FmResultSet=[database executeQuery:qryString];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            NSString *filterName = [FmResultSet stringForColumn:@"subfilter_name"];
            NSString *filterType = [FmResultSet stringForColumn:@"id"];
            [categoryMA addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:filterName,@"catetitle",filterType,@"cateid", nil]];
        }
    }
    return categoryMA;
}

/**
 *  Truncate all Table Data
 *
 *  @return Yes or No
 */
-(BOOL)truncateCategoryTable:(int)cate
{
FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
[database open];
    NSString *qryString=[NSString stringWithFormat:@"DELETE FROM category where filter_type=%d",cate];
BOOL success =  [database executeUpdate:qryString];
[database close];
return success;
}
/**
 *  Fetch Is Selected Category Values
 *
 *  @return filterName, FilterID
 */
-(NSMutableArray *)fetchIsSelectCategory
{
    NSMutableArray *isSelectMA=[[NSMutableArray alloc]init];
    FMDatabase *database=[FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qryString=@"select * from category where ischeck=1";
    FMResultSet *FmResultSet=[database executeQuery:qryString];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            NSString *filterName = [FmResultSet stringForColumn:@"filter_name"];
            NSString *filterType = [FmResultSet stringForColumn:@"id"];
            NSInteger type = [FmResultSet intForColumn:@"filter_type"];
            [isSelectMA addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:filterName,@"catetitle",filterType,@"cateid",[NSNumber numberWithInt:type],@"type", nil]];
        }
    }
    return isSelectMA;
}
/**
 *  Insert Category
 *  @param checkMA <#checkMA description#>
 *
 *  @return <#return value description#>
 */
-(BOOL)insertCategoryWithIscheck:(NSMutableArray *)checkMA
{
    BOOL successfullyInserted = FALSE;
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    for(int i=0;i<checkMA.count;i++)
    {
        successfullyInserted=[database executeUpdate:@"INSERT INTO category (filter_id,filter_name,filter_type,ischeck) VALUES (?,?,?,?)",[[checkMA objectAtIndex:i] objectForKey:@"filter_id"],[[checkMA objectAtIndex:i] objectForKey:@"filter_name"],[[checkMA objectAtIndex:i] objectForKey:@"filter_type"],@"1"];
    }
    if(successfullyInserted)
        successfullyInserted=TRUE;
    else
        
        [database close];
    
    
    return successfullyInserted;

}
-(BOOL)insertCategory:(NSMutableArray *)uncheckMA
{
    BOOL successfullyInserted = FALSE;

    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    for(int i=0;i<uncheckMA.count;i++)
    {
        successfullyInserted=[database executeUpdate:@"INSERT INTO location(location_id,location_name) VALUES (?,?)",[[uncheckMA objectAtIndex:i] objectForKey:@"id"],[[uncheckMA objectAtIndex:i] objectForKey:@"location"]];
    }
    if(successfullyInserted)
         successfullyInserted=TRUE;
    else
        
    [database close];
    
    
    return successfullyInserted;
}

-(BOOL)updateCategoryWithIscheck:(NSMutableArray *)checkMA
{
    NSLog(@"databasePath:: %@",databasePath);
    BOOL successfullyInserted = FALSE;
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    for(int i=0;i<checkMA.count;i++)
    {
        NSDictionary *dict = [checkMA objectAtIndex:i];
        NSLog(@"%d",[[dict objectForKey:@"isSelected"]integerValue]);
        NSLog(@"%d",[[dict objectForKey:@"cateid"]integerValue]);
        
        
        NSInteger isSelected = [[dict objectForKey:@"isSelected"]integerValue];
        NSInteger catId = [[dict objectForKey:@"cateid"]integerValue];
        
        
        NSString *query =  [NSString stringWithFormat:@"UPDATE category SET ischeck = '%ld' WHERE filter_id = %ld", (long)isSelected, (long)catId];
        successfullyInserted = [database executeUpdate:query];
        
    }
    
    
    if(successfullyInserted)
        successfullyInserted=TRUE;
    else
        
        [database close];
    
    
    return successfullyInserted;
    
}


-(NSMutableArray *)fetchLocation:(NSString*)str
{
    NSMutableArray *isSelectMA=[[NSMutableArray alloc]init];
    FMDatabase *database=[FMDatabase databaseWithPath:databasePath];
    [database open];
//    SELECT * FROM table WHERE location like '%kuw%'
   // NSString *qryString = [NSString stringWithFormat:@"select * from location where location like % %@ %",str];
    
   // FMResultSet *FmResultSet=[database executeQuery:qryString];
    
    FMResultSet *FmResultSet = [database executeQuery:@"SELECT * FROM location WHERE location_name LIKE ?",[NSString stringWithFormat:@"%%%@%%", str]];
    
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            
            NSString *filterType = [FmResultSet stringForColumn:@"location_name"];
            NSInteger type = [FmResultSet intForColumn:@"location_id"];
            [isSelectMA addObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:filterType,@"locationName",[NSNumber numberWithUnsignedInteger:type],@"locationID", nil]];
        }
    }
    return isSelectMA;
}

-(NSMutableArray *)fetchListForSelectedSubCategoryFromDB:(int)catid
{
    categoryMA=[[NSMutableArray alloc]init];
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qryString=[NSString stringWithFormat:@"select * from subcate where filter_id = %d and ischeck = 1",catid];
    FMResultSet *FmResultSet=[database executeQuery:qryString];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {

            NSString *filterid = [FmResultSet stringForColumn:@"subfilter_id"];
            
            [categoryMA addObject:[NSString stringWithFormat:@"%@",filterid]];
        }
    }
    return categoryMA;
}
-(BOOL)checkSubCategoryInDB:(NSMutableDictionary *)subCatDict
{
    BOOL findSubCat=false;
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qryString=[NSString stringWithFormat:@"select * from subcate where filter_id = %d and subfilter_id = %d",[[subCatDict objectForKey:@"filter_id"] integerValue],[[subCatDict objectForKey:@"sub_filter_id"] integerValue]];
    FMResultSet *FmResultSet=[database executeQuery:qryString];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            NSString *filterName = [FmResultSet stringForColumn:@"subfilter_name"];
            findSubCat=true;
            break;
        }
        [database close];
        return findSubCat;
    }
    [database close];
    return findSubCat;
}
-(BOOL)updateCategoryWithIscheckWhenSbCateSelected:(int)catId
{
    NSLog(@"databasePath:: %@",databasePath);
    BOOL successfullyInserted = FALSE;
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];

        NSString *query =  [NSString stringWithFormat:@"UPDATE category SET ischeck = '1' WHERE filter_id = %d",catId];
        successfullyInserted = [database executeUpdate:query];

    if(successfullyInserted)
        successfullyInserted=TRUE;
    else
        [database close];
    return successfullyInserted;
    
}
-(BOOL)updateCategoryWithIscheckWhenNoAnySbCateSelected:(int)catId
{
    NSLog(@"databasePath:: %@",databasePath);
    BOOL successfullyInserted = FALSE;
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *query =  [NSString stringWithFormat:@"UPDATE category SET ischeck = '0' WHERE filter_id = %d",catId];
    successfullyInserted = [database executeUpdate:query];
    
    if(successfullyInserted)
        successfullyInserted=TRUE;
    else
        [database close];
    return successfullyInserted;
}

-(BOOL)insertBookmark:(NSDictionary *)bookmarkDict
{
    BOOL successfullyInserted = FALSE;
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd, yyyy, hh:mm a"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[[bookmarkDict objectForKey:@"datetime"]doubleValue]];

    
    
    successfullyInserted=[database executeUpdate:@"INSERT INTO bookmark (nwsid,title,datetime,source,description,url,imgurl,ischeck) VALUES (?,?,?,?,?,?,?,?)",[bookmarkDict objectForKey:@"id"],[bookmarkDict objectForKey:@"title"],[dateFormatter stringFromDate:date],[bookmarkDict objectForKey:@"source"],[bookmarkDict objectForKey:@"description"],[bookmarkDict objectForKey:@"link"],[bookmarkDict objectForKey:@"imglink"],@"1"];
    
    if(successfullyInserted)
        successfullyInserted=TRUE;
    else
        
        [database close];
    
    
    return successfullyInserted;
    
}
-(BOOL)truncateLocationTableData
{
    BOOL successfullyRemove = FALSE;
    
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qry=[NSString stringWithFormat:@"DELETE FROM location"];
    successfullyRemove=[database executeUpdate:qry];
    
    if(successfullyRemove)
        successfullyRemove=TRUE;
    else
        
        [database close];
    
    
    return successfullyRemove;
}
-(BOOL)checkBookmark:(NSString *)nwsid
{
    BOOL successfullyInserted = FALSE;
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qry=[NSString stringWithFormat:@"select * from bookmark where nwsid=%@",nwsid];
    FMResultSet *FmResultSet=[database executeQuery:qry];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            
            successfullyInserted=true;
            return successfullyInserted;
            
        }
    }
    
    
    return successfullyInserted;
}
-(NSMutableArray *)fetchBookmarkDetails
{
    NSMutableArray *bookmarkArray=[[NSMutableArray alloc]init];
    
    FMDatabase *database=[FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qryString = [NSString stringWithFormat:@"select * from bookmark ORDER BY id DESC"];
    
    FMResultSet *FmResultSet=[database executeQuery:qryString];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            NSString *nwsid = [FmResultSet stringForColumn:@"nwsid"];
            NSString *title = [FmResultSet stringForColumn:@"title"];
            NSString *datetime = [FmResultSet stringForColumn:@"datetime"];
            NSString *source = [FmResultSet stringForColumn:@"source"];
            NSString *description = [FmResultSet stringForColumn:@"description"];
            NSString *url = [FmResultSet stringForColumn:@"url"];
            NSString *imgurl = [FmResultSet stringForColumn:@"imgurl"];
            NSString *ischeck = [FmResultSet stringForColumn:@"isCheck"];
            
            [bookmarkArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:nwsid,@"id",title,@"nwsTitle",datetime,@"nwsDateTime",source,@"nwsSource",description,@"nwsDesc",url,@"link",imgurl,@"nwsImgUrl",ischeck,@"nwsCheck", nil]];
        }
    }
    return bookmarkArray;
}

-(BOOL)checkSubCategoryValueInDB:(int)catId
{
    BOOL findSubCat=false;
    FMDatabase *database = [FMDatabase databaseWithPath:databasePath];
    [database open];
    
    NSString *qryString=[NSString stringWithFormat:@"select * from subcate where filter_id = %d",catId];
    FMResultSet *FmResultSet=[database executeQuery:qryString];
    if(FmResultSet)
    {
        while([FmResultSet next])
        {
            NSString *filterName = [FmResultSet stringForColumn:@"subfilter_name"];
            findSubCat=true;
            break;
        }
        [database close];
        return findSubCat;
    }
    [database close];
    return findSubCat;
}

@end
