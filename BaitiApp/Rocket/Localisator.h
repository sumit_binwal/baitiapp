//
//  Localisator.h
//  BaitiApp
//
//  Created by Chhagan Singh on 15/10/15.
//  Copyright © 2015 Shweta Rao. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LOCALIZATION(text) [[Localisator sharedInstance] localizedStringForKey:(text)]

static NSString * const kNotificationLanguageChanged = @"kNotificationLanguageChanged";


typedef NS_ENUM(NSInteger, LanguageType) {
    kArabic = 0,
    kEnglish
};


@interface Localisator : NSObject

@property (nonatomic, readonly) NSArray* availableLanguagesArray;

@property (nonatomic, copy,readonly) NSString * currentLanguage;

@property (nonatomic, assign,readonly) LanguageType currentLanguageType;

+ (Localisator*)sharedInstance;

-(NSString *)localizedStringForKey:(NSString*)key;

-(void)setNewLanguage:(LanguageType)newLanguage;
@end
