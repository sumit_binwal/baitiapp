//
//  TextFieldAlignment.m
//  RegistrationPage
//
//  Created by Shweta Rao on 11/02/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "TextFieldAlignment.h"

@implementation TextFieldAlignment

static CGFloat leftMargin = 18;

- (CGRect)textRectForBounds:(CGRect)bounds
{
    bounds.origin.x += leftMargin;
    
    return bounds;
}

- (CGRect)editingRectForBounds:(CGRect)bounds
{
    bounds.origin.x += leftMargin;
    
    return bounds;
}

//static CGFloat leftMargin;
//-(CGRect)textRectForBounds:(CGRect)bounds
//{
//    bounds.origin.x += leftMargin;
//    return bounds;
//}
//-(CGRect)editingRectForBounds:(CGRect)bounds
//{
//    bounds.origin.x += leftMargin;
//    return bounds;
//}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
