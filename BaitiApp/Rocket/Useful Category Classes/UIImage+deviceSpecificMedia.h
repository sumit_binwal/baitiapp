//
//  UIImage+deviceSpecificMedia.h
//  AlarmApp
//
//  Created by Chandan Kumar on 14/11/14.
//  Copyright (c) 2014 Konstant Infosolutions Pvt. Ltd. All rights reserved.
//

//  UIImage+deviceSpecificMedia.h
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSInteger, thisDeviceClass) {
    
    thisDeviceClass_iPhone,
    thisDeviceClass_iPhoneRetina,
    thisDeviceClass_iPhone5,
    thisDeviceClass_iPhone6,
    thisDeviceClass_iPhone6plus,
    
    
    thisDeviceClass_iPad,
    thisDeviceClass_iPadRetina,
    
    
    thisDeviceClass_unknown
};


thisDeviceClass currentDeviceClass();



@interface UIImage (deviceSpecificMedia)

+(instancetype )imageForDeviceWithName:(NSString *)fileName;

@end
