//
//  AppDelegate.m
//  BaitiApp
//
//  Created by Shweta Rao on 24/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "AppDelegate.h"
#import "SignInVC.h"
#import "HomeVC.h"
#import "MenuSideBarVC.h"
#import "NotificationsVC.h"



@interface AppDelegate ()
{
    UILocalNotification *remoteNotification;
    UIUserNotificationSettings *notificationSettings;
}
@end

@implementation AppDelegate
@synthesize currentLocationCoordinate,locationManager;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    remoteNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeNewsstandContentAvailability| UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    //[application registerForRemoteNotifications];
    
    locationManager = [[CLLocationManager alloc]init]; // initializing locationManager
    locationManager.delegate = self; // we set the delegate of locationManager to self.
    locationManager.desiredAccuracy = kCLLocationAccuracyBest; // setting the accuracy
    
    [locationManager startUpdatingLocation];

    
    LocalizationSystem *local=[LocalizationSystem shared];
   
    if ([[LocalizationSystem shared].language isEqualToString:@"ar"])
    {
        NSLog(@"------------------Arabic");
        language_id=@"ar";

    }
    else
    {
        language_id=@"en";
    }
        [[DBManager getSharedInstance]checkAndCreateDatabase];
    
    self.window=[[UIWindow alloc]initWithFrame:[UIScreen mainScreen].bounds];
    
        IIViewDeckController* deckController = [self generateControllerStack];

        [self.window setRootViewController:deckController];
        [self.window makeKeyAndVisible];
    // Override point for customization after application launch.
    return YES;
}

- (IIViewDeckController*)generateControllerStack
{
    MenuSideBarVC *rearViewController = [[MenuSideBarVC alloc]init];
    UIViewController *centerController;
    if (remoteNotification) {
    centerController = [[NotificationsVC alloc]initWithNibName:@"NotificationsVC" bundle:nil];
    }
    else
    {
        centerController = ICLocalizedViewController([HomeVC class]);//[[HomeVC alloc]initWithNibName:@"HomeVC" bundle:nil];
    }


    centerController = [[UINavigationController alloc] initWithRootViewController:centerController];
    IIViewDeckController* deckController =  [[IIViewDeckController alloc] initWithCenterViewController:centerController leftViewController:rearViewController rightViewController:nil];
    deckController.rightSize = 100;
    [deckController disablePanOverViewsOfClass:NSClassFromString(@"_UITableViewHeaderFooterContentView")];
    
    
    return deckController;
    
}


-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
  CurrentLatitude=newLocation.coordinate.latitude;
    CurrentLongitude=newLocation.coordinate.longitude;
    DLog(@"%@",currentLocation);
    [locationManager stopUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    DLog(@"didFailWithError: %@", error);
UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
userDeviceToken=[NSString stringWithFormat:@"%@",token];
    DLog(@"%@",userDeviceToken);
    NSLog(@"=========================================%@",userDeviceToken);
    }

+(AppDelegate*) getSharedInstance
{
    return (AppDelegate*)[[UIApplication sharedApplication]delegate];
}
-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    IIViewDeckController* deckController = [self generateControllerStack];
    [self.window setRootViewController:deckController];
    [self.window makeKeyAndVisible];

}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
//    [self saveContext];
}


@end
