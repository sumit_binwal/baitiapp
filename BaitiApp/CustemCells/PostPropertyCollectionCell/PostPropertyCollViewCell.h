//
//  AirValueCell.h
//  Blue
//
//  Created by Kshitij Godara on 07/02/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PostPropertyCollViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgVw;
@property (strong, nonatomic) IBOutlet UIButton *crossButton;



@end
