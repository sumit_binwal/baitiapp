//
//  NotificationsTableViewCell.h
//  BaitiApp
//
//  Created by Shweta Rao on 26/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationsTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *userProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *groupNameLbl;
@property (strong, nonatomic) IBOutlet UILabel *lblFavouritesName;
@property (strong, nonatomic) IBOutlet UIButton *btnAddFavourates;

@end
