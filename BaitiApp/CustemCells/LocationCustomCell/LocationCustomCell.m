//
//  LocationCustomCell.m
//  BaitiApp
//
//  Created by Sumit Sharma on 11/05/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "LocationCustomCell.h"

@implementation LocationCustomCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self) {
        self=(LocationCustomCell *)[[[NSBundle mainBundle] loadNibNamed:@"LocationCustomCell" owner:self options:nil] firstObject];

    }
    return self;
}
- (void)awakeFromNib {
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
