//
//  LocationCustomCell.h
//  BaitiApp
//
//  Created by Sumit Sharma on 11/05/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationCustomCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblName;

@end
