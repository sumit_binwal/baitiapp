//
//  SalesFilterCustomTableCell.m
//  BaitiApp
//
//  Created by Shweta Rao on 20/05/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "SalesFilterCustomTableCell.h"

@implementation SalesFilterCustomTableCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self=(SalesFilterCustomTableCell *)[[[NSBundle mainBundle] loadNibNamed:@"SalesFilterCustomTableCell" owner:self options:nil] firstObject];
        self.frame=CGRectMake(0.0f, 0.0f,[[UIScreen mainScreen] bounds].size.width, 40.0f);
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
