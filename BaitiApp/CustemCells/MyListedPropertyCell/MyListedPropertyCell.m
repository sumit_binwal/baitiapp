//
//  MyListedPropertyCell.m
//  BaitiApp
//
//  Created by Shweta Rao on 16/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "MyListedPropertyCell.h"

@implementation MyListedPropertyCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self=(MyListedPropertyCell *)[[[NSBundle mainBundle] loadNibNamed:@"MyListedPropertyCell" owner:self options:nil] firstObject];
        self.frame=CGRectMake(0.0f, 0.0f,[[UIScreen mainScreen] bounds].size.width, 73.0f);
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


@end
