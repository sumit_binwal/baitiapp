//
//  MyListedPropertyCell.h
//  BaitiApp
//
//  Created by Shweta Rao on 16/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "MGSwipeTableCell.h"
#import "MGSwipeTableCell.h"
#import <QuartzCore/QuartzCore.h>

@interface MyListedPropertyCell : MGSwipeTableCell

@property (strong, nonatomic) IBOutlet UIImageView *imageforBuilderName;
@property (strong, nonatomic) IBOutlet UILabel *lblForPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblForNumOfBedRooms;
@property (strong, nonatomic) IBOutlet UILabel *labelForLocationName;
@property (strong, nonatomic) IBOutlet UILabel *lblForTypeOfFlats;
@property (strong, nonatomic) IBOutlet UILabel *lblBed;


@end
