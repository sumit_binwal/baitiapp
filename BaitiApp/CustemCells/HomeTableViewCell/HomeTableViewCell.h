//
//  HomeTableViewCell.h
//  BaitiApp
//
//  Created by Shweta Rao on 25/03/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblBed;
@property (strong, nonatomic) IBOutlet UIImageView *BackGroundImageView;
@property (strong, nonatomic) IBOutlet UIImageView *TransparentImageView;
@property (strong, nonatomic) IBOutlet UILabel *labelForPrice;
@property (strong, nonatomic) IBOutlet UILabel *labelForNumOfRooms;
@property (strong, nonatomic) IBOutlet UILabel *labelForLocation;
@property (strong, nonatomic) IBOutlet UIImageView *imageforLocation;
@property (strong, nonatomic) IBOutlet UIButton *imageForLogo;
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UIButton *cameraButton;
@property (strong, nonatomic) IBOutlet UIButton *typesOfFlats;

@end
