//
//  MenuCustemCell.m
//  BaitiApp
//
//  Created by Shweta Rao on 04/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "MenuCustemCell.h"

@implementation MenuCustemCell
@synthesize favouritCountImg,favouritCountLbl,favouritImg,favouritLabl;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self=(MenuCustemCell *)[[[NSBundle mainBundle]loadNibNamed:@"MenuCustemCell" owner:self options:nil]objectAtIndex:0];
        
        
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
   // celltitelLbl.font = [UIFont fontWithName:@"AvenirLTStd-Light" size:17.0f];
    
    favouritCountImg.layer.cornerRadius = favouritCountImg.frame.size.width / 2;
    favouritCountImg.clipsToBounds = YES;

    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


@end
