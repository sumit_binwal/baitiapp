//
//  ViewContrTableCell.m
//  BaitiApp
//
//  Created by Shweta Rao on 12/06/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import "ViewContrTableCell.h"

@implementation ViewContrTableCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self=(ViewContrTableCell *)[[[NSBundle mainBundle] loadNibNamed:@"ViewContrTableCell" owner:self options:nil] firstObject];
        self.frame=CGRectMake(0.0f, 0.0f,[[UIScreen mainScreen] bounds].size.width, 44.0f);
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
