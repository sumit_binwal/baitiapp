//
//  ViewContrTableCell.h
//  BaitiApp
//
//  Created by Shweta Rao on 12/06/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewContrTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *label;

@end
