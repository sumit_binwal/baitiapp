//
//  DetailCustomCell.h
//  BaitiApp
//
//  Created by Shweta Rao on 20/04/15.
//  Copyright (c) 2015 Shweta Rao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailCustomCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *backGroundImage;
@property (strong, nonatomic) IBOutlet UILabel *labeLForStatus;
@property (strong, nonatomic) IBOutlet UILabel *labelForMeasurements;

@end
